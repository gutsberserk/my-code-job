"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
require("reflect-metadata");
const typeorm_1 = require("typeorm");
const app_1 = __importDefault(require("./app"));
const database_1 = __importDefault(require("./database"));
const dotenv_1 = __importDefault(require("dotenv"));
dotenv_1.default.config();
(0, typeorm_1.createConnection)(database_1.default)
    .then(() => {
    console.log("Database connected!");
    app_1.default.listen(process.env.PORT || 3001, () => {
        console.log("Server running!");
    });
})
    .catch((error) => console.log(error));
