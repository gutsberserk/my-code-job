"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.validateUpdateStage = void 0;
const Error_1 = require("../../utils/Error");
const validateUpdateStage = (req, res, next) => {
    const data = req.body;
    const availableKeys = ["name", "description", "status", "link"];
    const dataKeys = Object.keys(data);
    for (let i = 0; i < dataKeys.length; i++) {
        if (availableKeys.includes(dataKeys[i]) == false) {
            next(new Error_1.ErrorHandler(422, "Apenas as keys: " + availableKeys.join(", ") + " são disponíveis."));
        }
    }
    next();
};
exports.validateUpdateStage = validateUpdateStage;
