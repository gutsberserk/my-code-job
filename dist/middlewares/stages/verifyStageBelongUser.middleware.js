"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.verifyStageBelongUser = void 0;
const typeorm_1 = require("typeorm");
const uuid_validate_1 = __importDefault(require("uuid-validate"));
const entities_1 = require("../../entities");
const Error_1 = require("../../utils/Error");
const verifyStageBelongUser = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    const userId = req.userId;
    const stageId = req.params.id;
    if (!(0, uuid_validate_1.default)(userId)) {
        next(new Error_1.ErrorHandler(404, "User não encontrado!"));
    }
    if (!(0, uuid_validate_1.default)(stageId)) {
        next(new Error_1.ErrorHandler(404, "Stage não encontrado!"));
    }
    const userJobRepository = (0, typeorm_1.getRepository)(entities_1.UserJobs);
    const stageRepository = (0, typeorm_1.getRepository)(entities_1.Stage);
    const stage = yield stageRepository.findOne(stageId);
    if (!stage) {
        next(new Error_1.ErrorHandler(404, "Stage não enontrado!"));
    }
    const userIdBelong = yield userJobRepository.query(`
    SELECT "users"
    FROM "user_jobs"
    JOIN "stages" ON "stages"."userJobIdUuid" = "user_jobs"."uuid"
    JOIN "users" ON "users"."uuid" = "user_jobs"."userIdUuid"
    WHERE "stages"."uuid" = $1 ;
    `, [stageId]);
    if (userIdBelong.lenght == 0) {
        next(new Error_1.ErrorHandler(401, "Nenhuma operação pode ser realizada com este Stage já que não pertence ao User."));
    }
    if (userId != userIdBelong[0].uuid) {
        next(new Error_1.ErrorHandler(401, "Nenhuma operação pode ser realizada com este Stage já que não pertence ao User."));
    }
    next();
});
exports.verifyStageBelongUser = verifyStageBelongUser;
