"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.validateCreateUser = void 0;
const Error_1 = require("../../utils/Error");
const validateCreateUser = (req, res, next) => {
    const data = req.body;
    const requiredStage = [
        "name",
        "email",
        "password",
        "birthday",
        "cell_number",
        "gender",
    ];
    const missingKeys = [];
    for (let i = 0; i < requiredStage.length; i++) {
        if (Object.keys(data).includes(requiredStage[i]) == false) {
            missingKeys.push(requiredStage[i]);
        }
    }
    if (missingKeys.length > 0) {
        next(new Error_1.ErrorHandler(400, "As keys: " + missingKeys.join(", ") + " são necessárias."));
    }
    next();
};
exports.validateCreateUser = validateCreateUser;
