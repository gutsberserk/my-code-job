"use strict";
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.validatePartnerUpdate = void 0;
const validatePartnerUpdate = (req, res, next) => {
    const _a = req.body, { email, uuid } = _a, data = __rest(_a, ["email", "uuid"]);
    if (email || uuid) {
        res.status(401).json({
            message: "E-mail e uuid são atributos que não podem ser alterados.",
        });
    }
    else {
        next();
    }
};
exports.validatePartnerUpdate = validatePartnerUpdate;
