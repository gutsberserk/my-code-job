"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.validateUuidParamsPartners = void 0;
const Error_1 = require("../../utils/Error");
const uuid_validate_1 = __importDefault(require("uuid-validate"));
const validateUuidParamsPartners = (req, res, next) => {
    const { uuid } = req.params;
    if (!(0, uuid_validate_1.default)(uuid)) {
        next(new Error_1.ErrorHandler(404, "Partner não encontrado!"));
    }
    next();
};
exports.validateUuidParamsPartners = validateUuidParamsPartners;
