"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.validatePartnerKeys = void 0;
const validatePartnerKeys = (req, res, next) => {
    for (let key in req.body) {
        if (typeof req.body[key] !== "string") {
            res.status(400).json({
                message: "Todas as keys precisam ser string.",
            });
        }
    }
    next();
};
exports.validatePartnerKeys = validatePartnerKeys;
