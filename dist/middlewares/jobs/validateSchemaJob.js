"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.validateSchemaCreateJob = void 0;
const validateSchemaCreateJob = (schema) => (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    var _a;
    try {
        const data = req.body;
        try {
            const validatedData = yield schema.validate(data, {
                abortEarly: false,
                stripUnknown: true,
            });
            req.newJob = validatedData;
            next();
        }
        catch (err) {
            return res.status(400).json({
                error: (_a = err.errors) === null || _a === void 0 ? void 0 : _a.join(", "),
            });
        }
    }
    catch (err) {
        next(err);
    }
});
exports.validateSchemaCreateJob = validateSchemaCreateJob;
