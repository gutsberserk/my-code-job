"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.validateCreateJob = void 0;
const Error_1 = require("../../utils/Error");
const validateCreateJob = (req, res, next) => {
    const data = req.body;
    const requiredStage = [
        "name",
        "description",
        "location",
        "format",
        "hireMode",
        "link",
    ];
    const missingKeys = [];
    for (let i = 0; i < requiredStage.length; i++) {
        if (Object.keys(data).includes(requiredStage[i]) == false) {
            missingKeys.push(requiredStage[i]);
        }
    }
    if (missingKeys.length > 0) {
        next(new Error_1.ErrorHandler(400, "As keys " + missingKeys.join(", ") + " são necessárias."));
    }
    next();
};
exports.validateCreateJob = validateCreateJob;
