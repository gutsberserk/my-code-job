"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.validateUuidParamsJobs = void 0;
const uuid_validate_1 = __importDefault(require("uuid-validate"));
const Error_1 = require("../../utils/Error");
const validateUuidParamsJobs = (req, res, next) => {
    const { uuid } = req.params;
    if (!(0, uuid_validate_1.default)(uuid)) {
        next(new Error_1.ErrorHandler(404, "Job não encontrado!"));
    }
    next();
};
exports.validateUuidParamsJobs = validateUuidParamsJobs;
