"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.verifyJobExists = void 0;
const typeorm_1 = require("typeorm");
const uuid_validate_1 = __importDefault(require("uuid-validate"));
const entities_1 = require("../../entities");
const Error_1 = require("../../utils/Error");
const verifyJobExists = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    const jobId = req.params.id;
    if (!(0, uuid_validate_1.default)(jobId)) {
        next(new Error_1.ErrorHandler(404, "Job não encontrado!"));
    }
    const jobRepository = (0, typeorm_1.getRepository)(entities_1.Job);
    const job = yield jobRepository.findOne(jobId);
    if (!job) {
        next(new Error_1.ErrorHandler(404, "Job não encontrado!"));
    }
    next();
});
exports.verifyJobExists = verifyJobExists;
