"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.configRecover = exports.config = void 0;
exports.config = {
    secret: "secret_key",
    expiresIn: "24h",
};
exports.configRecover = {
    secret: "secret_key",
    expiresIn: "1h",
};
