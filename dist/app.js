"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const routes_1 = require("./routes");
const Error_1 = require("./utils/Error");
const app = (0, express_1.default)();
(0, routes_1.routesApp)(app);
app.use((err, _req, res, _next) => {
    (0, Error_1.handleError)(err, res);
});
exports.default = app;
