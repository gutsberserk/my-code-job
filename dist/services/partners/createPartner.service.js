"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const Error_1 = require("../../utils/Error");
const partners_repository_1 = require("../../repositories/partners.repository");
const users_repository_1 = __importDefault(require("../../repositories/users.repository"));
class CreatePartnerService {
    execute({ name, email, password, location, website }) {
        return __awaiter(this, void 0, void 0, function* () {
            const partnerRepository = (0, typeorm_1.getCustomRepository)(partners_repository_1.PartnersRepository);
            const userRepository = (0, typeorm_1.getCustomRepository)(users_repository_1.default);
            if (!email) {
                throw new Error_1.ErrorHandler(400, "É necessário inserir um e-mail válido.");
            }
            const emailAlreadyExistsInPartners = yield partnerRepository.findOne({
                email,
            });
            const emailAlreadyExists = yield userRepository.findOne({ email });
            if (emailAlreadyExists || emailAlreadyExistsInPartners) {
                throw new Error_1.ErrorHandler(409, "E-mail já registrado.");
            }
            const partner = partnerRepository.create({
                name,
                email,
                password_hash: password,
                location,
                website,
            });
            yield partnerRepository.save(partner);
            return partner;
        });
    }
}
exports.default = CreatePartnerService;
