"use strict";
var __awaiter =
  (this && this.__awaiter) ||
  function (thisArg, _arguments, P, generator) {
    function adopt(value) {
      return value instanceof P
        ? value
        : new P(function (resolve) {
            resolve(value);
          });
    }
    return new (P || (P = Promise))(function (resolve, reject) {
      function fulfilled(value) {
        try {
          step(generator.next(value));
        } catch (e) {
          reject(e);
        }
      }
      function rejected(value) {
        try {
          step(generator["throw"](value));
        } catch (e) {
          reject(e);
        }
      }
      function step(result) {
        result.done
          ? resolve(result.value)
          : adopt(result.value).then(fulfilled, rejected);
      }
      step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
  };
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const partners_repository_1 = require("../../repositories/partners.repository");
const Error_1 = require("../../utils/Error");
class DeletePartnerService {
  execute(uuid) {
    return __awaiter(this, void 0, void 0, function* () {
      const partnersRepository = (0, typeorm_1.getCustomRepository)(
        partners_repository_1.PartnersRepository
      );
      const partner = yield partnersRepository.findOne(uuid);
      if (partner === undefined) {
        throw new Error_1.ErrorHandler(404, "Partner não encontrado.");
      }
      yield partnersRepository.delete(uuid);
      return "Partner removido com sucesso!";
    });
  }
}
exports.default = DeletePartnerService;
