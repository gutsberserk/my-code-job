"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ListAllJobsPartnersService = exports.UpdatePartnerService = exports.RetrievePartnerByIdService = exports.ListAllPartnersService = exports.DeletePartnerService = exports.CreatePartnerService = void 0;
const createPartner_service_1 = __importDefault(require("./createPartner.service"));
exports.CreatePartnerService = createPartner_service_1.default;
const deletePartner_service_1 = __importDefault(require("./deletePartner.service"));
exports.DeletePartnerService = deletePartner_service_1.default;
const listAllPartners_service_1 = __importDefault(require("./listAllPartners.service"));
exports.ListAllPartnersService = listAllPartners_service_1.default;
const retrievePartnerById_service_1 = __importDefault(require("./retrievePartnerById.service"));
exports.RetrievePartnerByIdService = retrievePartnerById_service_1.default;
const updatePartner_service_1 = __importDefault(require("./updatePartner.service"));
exports.UpdatePartnerService = updatePartner_service_1.default;
const listAllJobsPartner_service_1 = __importDefault(require("./listAllJobsPartner.service"));
exports.ListAllJobsPartnersService = listAllJobsPartner_service_1.default;
