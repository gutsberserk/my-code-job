"use strict";
var __awaiter =
  (this && this.__awaiter) ||
  function (thisArg, _arguments, P, generator) {
    function adopt(value) {
      return value instanceof P
        ? value
        : new P(function (resolve) {
            resolve(value);
          });
    }
    return new (P || (P = Promise))(function (resolve, reject) {
      function fulfilled(value) {
        try {
          step(generator.next(value));
        } catch (e) {
          reject(e);
        }
      }
      function rejected(value) {
        try {
          step(generator["throw"](value));
        } catch (e) {
          reject(e);
        }
      }
      function step(result) {
        result.done
          ? resolve(result.value)
          : adopt(result.value).then(fulfilled, rejected);
      }
      step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
  };
var __importDefault =
  (this && this.__importDefault) ||
  function (mod) {
    return mod && mod.__esModule ? mod : { default: mod };
  };
Object.defineProperty(exports, "__esModule", { value: true });
const bcrypt_1 = __importDefault(require("bcrypt"));
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const typeorm_1 = require("typeorm");
const partners_repository_1 = require("../../repositories/partners.repository");
const users_repository_1 = __importDefault(
  require("../../repositories/users.repository")
);
const jwtConfig_1 = require("../../utils/jwtConfig");
const Error_1 = require("../../utils/Error");
class LoginService {
  execute({ email, password }) {
    return __awaiter(this, void 0, void 0, function* () {
      const userRepository = (0, typeorm_1.getCustomRepository)(
        users_repository_1.default
      );
      const partnersRepository = (0, typeorm_1.getCustomRepository)(
        partners_repository_1.PartnersRepository
      );
      const user = yield userRepository.findByEmail(email);
      if (user && bcrypt_1.default.compareSync(password, user.password_hash)) {
        const token = jsonwebtoken_1.default.sign(
          { id: user.uuid },
          jwtConfig_1.config.secret,
          {
            expiresIn: jwtConfig_1.config.expiresIn,
          }
        );
        return token;
      }
      const partner = yield partnersRepository.findOne({ email });
      if (
        partner &&
        bcrypt_1.default.compareSync(password, partner.password_hash)
      ) {
        const token = jsonwebtoken_1.default.sign(
          { id: partner.uuid },
          jwtConfig_1.config.secret,
          {
            expiresIn: jwtConfig_1.config.expiresIn,
          }
        );
        return token;
      }
      if (user === undefined && partner === undefined) {
        throw new Error_1.ErrorHandler(404, "E-mail não encontrado.");
      }
      if (
        (user && !bcrypt_1.default.compareSync(password, user.password_hash)) ||
        (partner &&
          !bcrypt_1.default.compareSync(password, partner.password_hash))
      ) {
        throw new Error_1.ErrorHandler(401, "Password incorreto.");
      }
    });
  }
}
exports.default = LoginService;
