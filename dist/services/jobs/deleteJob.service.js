"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const uuid_validate_1 = __importDefault(require("uuid-validate"));
const jobs_repository_1 = __importDefault(require("../../repositories/jobs.repository"));
const Error_1 = require("../../utils/Error");
class DeleteJobService {
    execute(uuid) {
        return __awaiter(this, void 0, void 0, function* () {
            const jobRepository = (0, typeorm_1.getCustomRepository)(jobs_repository_1.default);
            if (!(0, uuid_validate_1.default)(uuid)) {
                throw new Error_1.ErrorHandler(404, "Job não encontrado!");
            }
            const uuidRegex = /\b[0-9a-f]{8}\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\b[0-9a-f]{12}\b/gm;
            const isRegexValid = uuidRegex.test(uuid);
            if (!isRegexValid) {
                throw new Error_1.ErrorHandler(400, "Job não está formatado!");
            }
            const job = yield jobRepository.findOne(uuid);
            if (!job) {
                throw new Error_1.ErrorHandler(404, "Job não encontrado!");
            }
            yield jobRepository.delete(uuid);
            return "Job removido com sucesso!";
        });
    }
}
exports.default = DeleteJobService;
