"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const uuid_validate_1 = __importDefault(require("uuid-validate"));
const entities_1 = require("../../entities");
const Error_1 = require("../../utils/Error");
class UnsubscribeJobInUserService {
    execute(userId, jobId) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!(0, uuid_validate_1.default)(jobId)) {
                throw new Error_1.ErrorHandler(404, "Job não encontrado!");
            }
            if (!(0, uuid_validate_1.default)(userId)) {
                throw new Error_1.ErrorHandler(404, "User não encontrado!");
            }
            const userJobsRepository = (0, typeorm_1.getRepository)(entities_1.UserJobs);
            const userRepository = (0, typeorm_1.getRepository)(entities_1.User);
            const jobRepository = (0, typeorm_1.getRepository)(entities_1.Job);
            const user = yield userRepository.findOne(userId);
            if (!user) {
                throw new Error_1.ErrorHandler(404, "User não encontrado!");
            }
            const job = yield jobRepository.findOne(jobId);
            if (!job) {
                throw new Error_1.ErrorHandler(404, "Job não encontrado!");
            }
            const userJob = yield userJobsRepository.findOne({
                where: {
                    userId: user,
                    jobId: job,
                },
            });
            if (!userJob) {
                throw new Error_1.ErrorHandler(404, "O(a) User não está registrado(a) neste Job.");
            }
            yield userJobsRepository.delete(userJob.uuid);
        });
    }
}
exports.default = UnsubscribeJobInUserService;
