"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const uuid_validate_1 = __importDefault(require("uuid-validate"));
const jobs_repository_1 = __importDefault(require("../../repositories/jobs.repository"));
const Error_1 = require("../../utils/Error");
class UpdateJobService {
    execute({ uuid, name, description, plataform, location, salary, benefits, format, requiredTimeOfXP, requiredXP, differentialXP, hireMode, link, hasTest, }) {
        return __awaiter(this, void 0, void 0, function* () {
            const jobsRepository = (0, typeorm_1.getCustomRepository)(jobs_repository_1.default);
            if (!(0, uuid_validate_1.default)(uuid)) {
                throw new Error_1.ErrorHandler(404, "Job não encontrado!");
            }
            const uuidRegex = /\b[0-9a-f]{8}\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\b[0-9a-f]{12}\b/gm;
            const isRegexValid = uuidRegex.test(uuid);
            if (!isRegexValid) {
                throw new Error_1.ErrorHandler(400, "Job id não está formatado!");
            }
            const job = yield jobsRepository.findOne({ uuid });
            if (!job) {
                throw new Error_1.ErrorHandler(404, "Job não encontrado!");
            }
            name ? (job.name = name) : job.name;
            description ? (job.description = description) : job.description;
            plataform ? (job.plataform = plataform) : job.plataform;
            location ? (job.location = location) : job.location;
            salary ? (job.salary = salary) : job.salary;
            benefits ? (job.benefits = benefits) : job.benefits;
            format ? (job.format = format) : job.format;
            requiredTimeOfXP
                ? (job.requiredTimeOfXP = requiredTimeOfXP)
                : job.requiredTimeOfXP;
            requiredXP ? (job.requiredXP = requiredXP) : job.requiredXP;
            differentialXP ? (job.differentialXP = differentialXP) : job.differentialXP;
            hireMode ? (job.hireMode = hireMode) : job.hireMode;
            link ? (job.link = link) : job.link;
            hasTest ? (job.hasTest = hasTest) : job.hasTest;
            jobsRepository.save(job);
            return job;
        });
    }
}
exports.default = UpdateJobService;
