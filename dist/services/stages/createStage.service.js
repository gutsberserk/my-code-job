"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const uuid_validate_1 = __importDefault(require("uuid-validate"));
const entities_1 = require("../../entities");
const Error_1 = require("../../utils/Error");
class CreateStageService {
    execute(jobId, userId, data) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!(0, uuid_validate_1.default)(jobId)) {
                throw new Error_1.ErrorHandler(404, "Job não encontrado!");
            }
            if (!(0, uuid_validate_1.default)(userId)) {
                throw new Error_1.ErrorHandler(404, "User não encontrado!");
            }
            const userJobsRepository = (0, typeorm_1.getRepository)(entities_1.UserJobs);
            const stageRepository = (0, typeorm_1.getRepository)(entities_1.Stage);
            const userJob = yield userJobsRepository.findOne({
                where: {
                    userId: userId,
                    jobId: jobId,
                },
            });
            if (!userJob) {
                throw new Error_1.ErrorHandler(404, "User não foi registrado neste Job.");
            }
            const stage = stageRepository.create({
                userJobId: userJob,
                name: data.name,
                description: data.description,
                link: data.link,
                status: data.status,
                scheduleDate: data.scheduleDate,
            });
            yield stageRepository.save(stage);
            const { userJobId } = stage, dataStage = __rest(stage, ["userJobId"]);
            const showStage = Object.assign(Object.assign({}, dataStage), { userJobId: userJobId.uuid });
            return showStage;
        });
    }
}
exports.default = CreateStageService;
