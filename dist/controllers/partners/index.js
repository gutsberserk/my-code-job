"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ListAllJobsPartnersController = exports.RetrievePartnerByIdController = exports.UpdatePartnerController = exports.ListAllPartnersController = exports.DeletePartnerController = exports.CreatePartnerController = void 0;
const createPartner_controller_1 = __importDefault(require("./createPartner.controller"));
exports.CreatePartnerController = createPartner_controller_1.default;
const deletePartner_controller_1 = __importDefault(require("./deletePartner.controller"));
exports.DeletePartnerController = deletePartner_controller_1.default;
const listAllPartners_controller_1 = __importDefault(require("./listAllPartners.controller"));
exports.ListAllPartnersController = listAllPartners_controller_1.default;
const updatePartner_controller_1 = __importDefault(require("./updatePartner.controller"));
exports.UpdatePartnerController = updatePartner_controller_1.default;
const retrievePartnerById_controller_1 = __importDefault(require("./retrievePartnerById.controller"));
exports.RetrievePartnerByIdController = retrievePartnerById_controller_1.default;
const listAllJobsPartners_controller_1 = __importDefault(require("./listAllJobsPartners.controller"));
exports.ListAllJobsPartnersController = listAllJobsPartners_controller_1.default;
