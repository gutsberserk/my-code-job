"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const users_1 = require("../../services/users");
class DeleteUserController {
    handle(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { uuid } = request.params;
                const deleteUserService = new users_1.DeleteUserService();
                const partner = yield deleteUserService.execute(uuid);
                return response
                    .status(200)
                    .json({ message: "User removido com sucesso!" });
            }
            catch (err) {
                next(err);
            }
        });
    }
}
exports.default = DeleteUserController;
