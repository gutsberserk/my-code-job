"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const jobs_1 = require("../../services/jobs");
const Error_1 = require("../../utils/Error");
class RetrieveJobByIdController {
    handle(request, response) {
        return __awaiter(this, void 0, void 0, function* () {
            const { uuid } = request.params;
            const jobById = new jobs_1.RetrieveJobByIdService();
            try {
                const job = yield jobById.execute(uuid);
                return response.json(job);
            }
            catch (err) {
                if (err instanceof Error_1.ErrorHandler) {
                    (0, Error_1.handleError)(err, response);
                }
            }
        });
    }
}
exports.default = RetrieveJobByIdController;
