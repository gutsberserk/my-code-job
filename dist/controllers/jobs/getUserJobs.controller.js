"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GetUserJobsController = void 0;
const typeorm_1 = require("typeorm");
const entities_1 = require("../../entities");
const Error_1 = require("../../utils/Error");
const GetUserJobsController = (request, response, next) => __awaiter(void 0, void 0, void 0, function* () {
    const id = request.userId;
    try {
        const userJobsRepository = (0, typeorm_1.getRepository)(entities_1.UserJobs);
        const userJob = yield userJobsRepository.find({
            where: {
                userId: id,
            },
        });
        response.send({ data: userJob });
    }
    catch (err) {
        if (err instanceof Error_1.ErrorHandler) {
            (0, Error_1.handleError)(err, response);
        }
    }
});
exports.GetUserJobsController = GetUserJobsController;
