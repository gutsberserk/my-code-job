"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const createJobs_service_1 = __importDefault(require("../../services/jobs/createJobs.service"));
const Error_1 = require("../../utils/Error");
class CreateJobController {
    handle(request, response) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { name, description, plataform, location, salary, benefits, format, requiredTimeOfXP, requiredXP, differentialXP, hireMode, link, hasTest, } = request.body;
                const createJobService = new createJobs_service_1.default();
                const job = yield createJobService.execute({
                    name,
                    description,
                    plataform,
                    location,
                    salary,
                    benefits,
                    format,
                    requiredTimeOfXP,
                    requiredXP,
                    differentialXP,
                    hireMode,
                    link,
                    hasTest,
                });
                return response.status(201).json(job);
            }
            catch (err) {
                if (err instanceof Error_1.ErrorHandler) {
                    (0, Error_1.handleError)(err, response);
                }
            }
        });
    }
}
exports.default = CreateJobController;
