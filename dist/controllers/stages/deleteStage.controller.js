"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const stages_1 = require("../../services/stages");
class DeleteStageController {
    handle(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const stageId = request.params.id;
                const deleteStageSerice = new stages_1.DeleteStageService();
                yield deleteStageSerice.execute(stageId);
                response.send({ message: "Stage foi removido." });
            }
            catch (err) {
                next(err);
            }
        });
    }
}
exports.default = DeleteStageController;
