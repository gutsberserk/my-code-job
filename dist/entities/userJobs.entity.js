"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserJobs = void 0;
const typeorm_1 = require("typeorm");
const _1 = require(".");
let UserJobs = class UserJobs {
};
__decorate([
    (0, typeorm_1.PrimaryGeneratedColumn)("uuid"),
    __metadata("design:type", String)
], UserJobs.prototype, "uuid", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], UserJobs.prototype, "status", void 0);
__decorate([
    (0, typeorm_1.UpdateDateColumn)(),
    __metadata("design:type", Date)
], UserJobs.prototype, "last_update", void 0);
__decorate([
    (0, typeorm_1.CreateDateColumn)(),
    __metadata("design:type", Date)
], UserJobs.prototype, "createdOn", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(type => _1.User, user => user.uuid),
    __metadata("design:type", _1.User)
], UserJobs.prototype, "userId", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(type => _1.Job, job => job.uuid),
    __metadata("design:type", _1.Job)
], UserJobs.prototype, "jobId", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(type => _1.Stage, stage => stage.userJobId, { eager: true }),
    __metadata("design:type", Array)
], UserJobs.prototype, "stages", void 0);
UserJobs = __decorate([
    (0, typeorm_1.Entity)()
], UserJobs);
exports.UserJobs = UserJobs;
