"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.loginRoute = void 0;
const express_1 = require("express");
const login_controller_1 = __importDefault(require("../controllers/login/login.controller"));
const middlewares_1 = require("../middlewares");
const LoginSchema_1 = __importDefault(require("../schemas/LoginSchema"));
const route = (0, express_1.Router)();
const loginController = new login_controller_1.default();
const loginRoute = (app) => {
    route.post("", (0, middlewares_1.validateSchemaLogin)(LoginSchema_1.default), loginController.handle);
    app.use("/login", route);
};
exports.loginRoute = loginRoute;
