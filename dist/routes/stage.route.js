"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.stageRoute = void 0;
const express_1 = require("express");
const stages_1 = require("../controllers/stages");
const updateStage_controllers_1 = __importDefault(require("../controllers/stages/updateStage.controllers"));
const middlewares_1 = require("../middlewares");
const route = (0, express_1.Router)();
const createStageController = new stages_1.CreateStageController();
const updateStageController = new updateStage_controllers_1.default();
const retrieveStageByIdController = new stages_1.RetrieveStageByIdController();
const listAllStagesController = new stages_1.ListAllStagesController();
const deleteStageController = new stages_1.DeleteStageController();
const stageRoute = (app) => {
    route.post("/job/:id", middlewares_1.isLogged, middlewares_1.verifyJobExists, middlewares_1.validateCreateStage, createStageController.handle);
    route.patch("/:id", middlewares_1.validateUpdateStage, updateStageController.handle);
    route.get("/:id", middlewares_1.isLogged, retrieveStageByIdController.handle);
    route.get("/job/:id", middlewares_1.isLogged, middlewares_1.verifyJobExists, listAllStagesController.handle);
    route.delete("/:id", middlewares_1.isLogged, middlewares_1.verifyStageBelongUser, deleteStageController.handle);
    app.use("/stage", route);
};
exports.stageRoute = stageRoute;
