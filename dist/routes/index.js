"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.routesApp = void 0;
const express_1 = __importDefault(require("express"));
const doc_route_1 = require("./doc.route");
const job_route_1 = require("./job.route");
const login_route_1 = require("./login.route");
const partner_route_1 = require("./partner.route");
const stage_route_1 = require("./stage.route");
const user_route_1 = require("./user.route");
const routesApp = (app) => {
    app.use(express_1.default.json());
    (0, login_route_1.loginRoute)(app);
    (0, user_route_1.userRoute)(app);
    (0, partner_route_1.partnerRoute)(app);
    (0, job_route_1.jobRoute)(app);
    (0, stage_route_1.stageRoute)(app);
    (0, login_route_1.loginRoute)(app);
    (0, doc_route_1.docRoute)(app);
};
exports.routesApp = routesApp;
