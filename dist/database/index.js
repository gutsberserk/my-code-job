"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const dotenv_1 = __importDefault(require("dotenv"));
dotenv_1.default.config();
const config = {
    type: "postgres",
    url: process.env.DATABASE_URL,
    synchronize: true,
    logging: false,
    cli: {
        entitiesDir: "src/entities",
        migrationsDir: "src/migrations",
    },
    ssl: process.env.NODE_ENV === "production"
        ? { rejectUnauthorized: false }
        : false,
    entities: process.env.NODE_ENV === "production"
        ? ["dist/entities/**/*.js"]
        : ["src/entities/**/*.ts"],
    migrations: process.env.NODE_ENV === "production"
        ? ["dist/migrations/**/*.js"]
        : ["src/migrations/**/*.ts"],
};
exports.default = config;
