"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.alterTables1645102053820 = void 0;
class alterTables1645102053820 {
    constructor() {
        this.name = 'alterTables1645102053820';
    }
    up(queryRunner) {
        return __awaiter(this, void 0, void 0, function* () {
            yield queryRunner.query(`ALTER TABLE "stages" DROP CONSTRAINT "FK_d880fe7afcecd05156abd25d3dc"`);
            yield queryRunner.query(`ALTER TABLE "users" ADD "name" character varying NOT NULL`);
            yield queryRunner.query(`ALTER TABLE "user_jobs" DROP CONSTRAINT "FK_5363e9599342fc3fe265d89fab9"`);
            yield queryRunner.query(`ALTER TABLE "users" DROP CONSTRAINT "PK_951b8f1dfc94ac1d0301a14b7e1"`);
            yield queryRunner.query(`ALTER TABLE "users" DROP COLUMN "uuid"`);
            yield queryRunner.query(`ALTER TABLE "users" ADD "uuid" uuid NOT NULL DEFAULT uuid_generate_v4()`);
            yield queryRunner.query(`ALTER TABLE "users" ADD CONSTRAINT "PK_951b8f1dfc94ac1d0301a14b7e1" PRIMARY KEY ("uuid")`);
            yield queryRunner.query(`ALTER TABLE "user_jobs" DROP COLUMN "userIdUuid"`);
            yield queryRunner.query(`ALTER TABLE "user_jobs" ADD "userIdUuid" uuid`);
            yield queryRunner.query(`ALTER TABLE "user_jobs" ADD CONSTRAINT "FK_5363e9599342fc3fe265d89fab9" FOREIGN KEY ("userIdUuid") REFERENCES "users"("uuid") ON DELETE NO ACTION ON UPDATE NO ACTION`);
            yield queryRunner.query(`ALTER TABLE "stages" ADD CONSTRAINT "FK_7245e3039e535ffa1e7786cb490" FOREIGN KEY ("userJobIdUuid") REFERENCES "user_jobs"("uuid") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        });
    }
    down(queryRunner) {
        return __awaiter(this, void 0, void 0, function* () {
            yield queryRunner.query(`ALTER TABLE "stages" DROP CONSTRAINT "FK_7245e3039e535ffa1e7786cb490"`);
            yield queryRunner.query(`ALTER TABLE "user_jobs" DROP CONSTRAINT "FK_5363e9599342fc3fe265d89fab9"`);
            yield queryRunner.query(`ALTER TABLE "user_jobs" DROP COLUMN "userIdUuid"`);
            yield queryRunner.query(`ALTER TABLE "user_jobs" ADD "userIdUuid" character varying`);
            yield queryRunner.query(`ALTER TABLE "users" DROP CONSTRAINT "PK_951b8f1dfc94ac1d0301a14b7e1"`);
            yield queryRunner.query(`ALTER TABLE "users" DROP COLUMN "uuid"`);
            yield queryRunner.query(`ALTER TABLE "users" ADD "uuid" character varying NOT NULL`);
            yield queryRunner.query(`ALTER TABLE "users" ADD CONSTRAINT "PK_951b8f1dfc94ac1d0301a14b7e1" PRIMARY KEY ("uuid")`);
            yield queryRunner.query(`ALTER TABLE "user_jobs" ADD CONSTRAINT "FK_5363e9599342fc3fe265d89fab9" FOREIGN KEY ("userIdUuid") REFERENCES "users"("uuid") ON DELETE NO ACTION ON UPDATE NO ACTION`);
            yield queryRunner.query(`ALTER TABLE "users" DROP COLUMN "name"`);
            yield queryRunner.query(`ALTER TABLE "stages" ADD CONSTRAINT "FK_d880fe7afcecd05156abd25d3dc" FOREIGN KEY ("userJobIdUuid") REFERENCES "user_jobs"("uuid") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        });
    }
}
exports.alterTables1645102053820 = alterTables1645102053820;
