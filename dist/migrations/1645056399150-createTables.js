"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.createTables1645056399150 = void 0;
class createTables1645056399150 {
    constructor() {
        this.name = "createTables1645056399150";
    }
    up(queryRunner) {
        return __awaiter(this, void 0, void 0, function* () {
            yield queryRunner.query(`CREATE TABLE "partners" ("uuid" character varying NOT NULL, "email" character varying NOT NULL, "name" character varying NOT NULL, "password_hash" character varying NOT NULL, "location" character varying NOT NULL, "website" character varying NOT NULL, CONSTRAINT "UQ_6b39bc13ab676e74eada2e744db" UNIQUE ("email"), CONSTRAINT "PK_189b05b18cdd50ab216ba7138b0" PRIMARY KEY ("uuid"))`);
            yield queryRunner.query(`CREATE TABLE "jobs" ("uuid" character varying NOT NULL, "name" character varying NOT NULL, "description" character varying NOT NULL, "plataform" character varying NOT NULL, "location" character varying NOT NULL, "salary" character varying NOT NULL, "benefits" character varying NOT NULL, "format" character varying NOT NULL, "requiredTimeOfXP" character varying NOT NULL, "requiredXP" character varying NOT NULL, "differentialXP" character varying NOT NULL, "hireMode" character varying NOT NULL, "link" character varying NOT NULL, "hasTest" boolean NOT NULL, "partnerIdUuid" character varying, CONSTRAINT "PK_2ad99c480880ac224b7e39338ba" PRIMARY KEY ("uuid"))`);
            yield queryRunner.query(`CREATE TABLE "users" ("uuid" character varying NOT NULL, "email" character varying NOT NULL, "password_hash" character varying NOT NULL, "birthday" TIMESTAMP NOT NULL, "gender" character varying NOT NULL, CONSTRAINT "UQ_97672ac88f789774dd47f7c8be3" UNIQUE ("email"), CONSTRAINT "PK_951b8f1dfc94ac1d0301a14b7e1" PRIMARY KEY ("uuid"))`);
            yield queryRunner.query(`CREATE TABLE "user_jobs" ("uuid" uuid NOT NULL DEFAULT uuid_generate_v4(), "status" character varying NOT NULL, "last_update" TIMESTAMP NOT NULL DEFAULT now(), "createdOn" TIMESTAMP NOT NULL DEFAULT now(), "userIdUuid" character varying, "jobIdUuid" character varying, CONSTRAINT "PK_855478bcadafa370d1378424501" PRIMARY KEY ("uuid"))`);
            yield queryRunner.query(`CREATE TABLE "stages" ("uuid" uuid NOT NULL DEFAULT uuid_generate_v4(), "name" character varying NOT NULL, "description" character varying NOT NULL, "created_on" TIMESTAMP NOT NULL DEFAULT now(), "last_update" TIMESTAMP NOT NULL DEFAULT now(), "link" character varying NOT NULL, "scheduleDate" TIMESTAMP NOT NULL, "status" character varying NOT NULL, "userJobIdUuid" uuid, CONSTRAINT "PK_d8506306e35bf694dd56c1900e8" PRIMARY KEY ("uuid"))`);
            yield queryRunner.query(`ALTER TABLE "jobs" ADD CONSTRAINT "FK_54b90a0af63272b01765c4750b5" FOREIGN KEY ("partnerIdUuid") REFERENCES "partners"("uuid") ON DELETE NO ACTION ON UPDATE NO ACTION`);
            yield queryRunner.query(`ALTER TABLE "user_jobs" ADD CONSTRAINT "FK_5363e9599342fc3fe265d89fab9" FOREIGN KEY ("userIdUuid") REFERENCES "users"("uuid") ON DELETE NO ACTION ON UPDATE NO ACTION`);
            yield queryRunner.query(`ALTER TABLE "user_jobs" ADD CONSTRAINT "FK_1d7997bf661233c4af608962f5c" FOREIGN KEY ("jobIdUuid") REFERENCES "jobs"("uuid") ON DELETE NO ACTION ON UPDATE NO ACTION`);
            yield queryRunner.query(`ALTER TABLE "stages" ADD CONSTRAINT "FK_d880fe7afcecd05156abd25d3dc" FOREIGN KEY ("userJobIdUuid") REFERENCES "user_jobs"("uuid") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        });
    }
    down(queryRunner) {
        return __awaiter(this, void 0, void 0, function* () {
            yield queryRunner.query(`ALTER TABLE "stagess" DROP CONSTRAINT "FK_d880fe7afcecd05156abd25d3dc"`);
            yield queryRunner.query(`ALTER TABLE "user_jobs" DROP CONSTRAINT "FK_1d7997bf661233c4af608962f5c"`);
            yield queryRunner.query(`ALTER TABLE "user_jobs" DROP CONSTRAINT "FK_5363e9599342fc3fe265d89fab9"`);
            yield queryRunner.query(`ALTER TABLE "jobs" DROP CONSTRAINT "FK_54b90a0af63272b01765c4750b5"`);
            yield queryRunner.query(`DROP TABLE "stages"`);
            yield queryRunner.query(`DROP TABLE "user_jobs"`);
            yield queryRunner.query(`DROP TABLE "users"`);
            yield queryRunner.query(`DROP TABLE "jobs"`);
            yield queryRunner.query(`DROP TABLE "partners"`);
        });
    }
}
exports.createTables1645056399150 = createTables1645056399150;
