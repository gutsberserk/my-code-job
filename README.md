<h1 align="center" id="top">Backend App <span style="color:#10BEF5">MyCodeJob</span></h1>
<p align="center">MyCodeJob</p>
<br>
<div align="center">
  <img src="https://i.imgur.com/tYxV2LU.png" style="border-radius: 4%" width="400px" alt="Logo MyCodeJob"/>
</div>
<br>
<p align="center">
<h3 align="left">
<span style="color:#10BEF5"><em>MyCodeJobs</em></span> é uma aplicação de gerenciamento que organiza vagas e candidaturas.
</h3>

## 🕹 Features

Todas as features podem ser encontradas em nossa documentação que pode ser acessa através deste link:

https://mycodejob.herokuapp.com/api-documentation

## ⚠ Antes de iniciar

Antes de começar, você vai precisar ter instalado em sua máquina as seguintes ferramentas:

- [Git](https://git-scm.com)
- [NodeJs](https://nodejs.org/en/)
- [Express](https://expressjs.com/)
- [Typescript](https://www.typescriptlang.org/)
- [Postgres](https://www.postgresql.org/)

E uma ferramenta operar a aplicação, sugerirmos o VS Code.
[VSCode](https://code.visualstudio.com/).

TypeORM possui uma ferramenta CLI que nos permite gerar a aplicação base direto em Typescript. Para usá-lo, só instalá-lo vai terminal:

```bash
npm install -g typeorm
```

## 📦 Requerimentos

Node, express, TypeORM, reflect-metadata, jsonwebtoken, bcrypt, postgres, nodemon, dotenv, ts-node-dev, jest and super-test, swagger, yup e sucrase.

## 🚦 Avisos

Por favor, para uma maior estabilidade, use node versão 14 ou 16.

SE você não sabe quão versão está em uso na sua máquina, digite isto em seu terminal:

```bash
node --version
```

SE você deseja mudar a versão do Node,
digite isto em seu terminal:

```bash
nvm use 14
```

<h3>OU</h3>

```bash
nvm use 16
```

Verifique se não há nenhuma outra aplicação que está sendo executada na porta :3000 e na porta :5432

Para ter certeza de que nada está sendo executado nestas portas, digite em seu terminal:

```bash
sudo lsof -i :3000
```

```bash
sudo lsof -i :5432
```

Se você encontrar alguma aplicação sendo executada, termine o processo com o seguinte comando(digite o PID):

```bash
sudo kill -9 PID
```

## 🎲 Instalação

Para iniciar o projeto na sua máquina, é necessário que sua máquina tenha instalado yarn e o git, além de uma versão do node compativel.

Começe clonando o repositório usando git clone:

```
git clone https://gitlab.com/gutsberserk/my-code-job
```

Depois disso, digite <em>yarn</em> (ou <em>yarn install</em>) para instalar todas as dependências:

```bash
yarn
```

Também é preciso criar um `.env` com as informações do banco de dados e outras informações necessárias conforme o `.env.example`.

Para iniciar a aplicação em sua máquina local, digite em seu terminal:

```bash
yarn dev
```

### <h2> 🛠 Tecnologias </h2>

As seguintes ferramentas foram usadas na construção do projeto:

- [DBeaver](https://dbeaver.io)
- [Heroku](https://heroku.com)
- [Insomnia](https://insomnia.rest)
- [Postgresql](https://www.postgresql.org/)
- [Slack](https://slack.com)
- [Trello](https://trello.com)
- [Vercel](https://vercel.com)
- [VSCode](https://code.visualstudio.com)
- [Zoom](https://explore.zoom.us)

### <h2> 📋 Termos de uso </h2>

<p>Este projeto é um projeto <em>opensource</em> com fins educacionais e não possui nenhuma finalidade comercial.</p>

<div align="center">
  <a href="https://choosealicense.com/licenses/mit/" target="_blank"><img src="https://img.shields.io/static/v1?label=License&message=MIT&color=informational"></a>
 </div>

<h2 id="desenvolvedores">🧑‍💻 Equipe de Desenvolvimento</h2>
<br>   
<div align="center">
<table align="center">
  <tr>
    <td align="center"><a href="https://gitlab.com/IgorPetersson">
      <img src="https://ca.slack-edge.com/TQZR39SET-U01QNUDCN7M-24007b058eea-512" style="border-radius: 50%" width="100px" alt="Imagem do perfil de Igor"/>
      <br />
      <sub><b>Igor P. Cardoso e Santos</b></sub>
      <br />
      <sub>PO, Product Owner</sub>
      <br />
    </td>
    <td align="center"><a href="https://gitlab.com/osmaren">
      <img src="https://avatars.githubusercontent.com/u/82000434?v=4" style="border-radius: 50%" width="100px" alt="Imagem do perfil de Osmar"/>
      <br />
      <sub><b>Osmar E. Nothaft</b></sub>
      <br />
      <sub>SM, Scrum Master</sub>
      <br />
    </td>
    <td align="center"><a href="https://gitlab.com/osmarn">
      <img src="https://ca.slack-edge.com/TQZR39SET-U01RWJW604S-6000e511a184-512" style="border-radius: 50%" width="100px" alt="Imagem do perfil de Lucas Ribeiro"/>
      <br />
      <sub><b>Lucas Ribeiro</b></sub>
      <br />
      <sub>TL, Tech Leader</sub>
      <br />
    </td>
    <td align="center"><a href="https://gitlab.com/andre-rosas">
      <img src="https://ca.slack-edge.com/TQZR39SET-U01SH3TE3UL-63b11c31f2b6-512" style="border-radius: 50%" width="100px" alt="Imagem do perfil de Osmar"/>
      <br />
      <sub><b>André Rosas</b></sub>
      <br />
      <sub>Dev, Desenvolvedor</sub>
      <br />
    </td> 
</table>
</div>

[Voltar para o topo 🔝](#top)
