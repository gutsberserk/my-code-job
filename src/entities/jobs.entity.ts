import { v4 as uuid } from "uuid";
import { Entity, PrimaryColumn, Column, ManyToOne, OneToMany } from "typeorm";

import { Partner, User, UserJobs } from ".";

@Entity("jobs")
export class Job {
  findOne(arg0: { uuid: string }) {
    throw new Error("Método não implementado.");
  }

  @PrimaryColumn()
  readonly uuid: string;

  @Column({ nullable: false })
  name: string;

  @Column({ nullable: false })
  description: string;

  @Column()
  plataform: string;

  @Column({ nullable: false })
  location: string;

  @Column()
  salary: string;

  @Column()
  benefits: string;

  @Column({ nullable: false })
  format: string;

  @Column()
  requiredTimeOfXP: string;

  @Column()
  requiredXP: string;

  @Column()
  differentialXP: string;

  @Column({ nullable: false })
  hireMode: string;

  @Column({ nullable: false })
  link: string;

  @Column()
  hasTest: boolean;

  @ManyToOne((type) => Partner, (partner) => partner.uuid)
  partnerId: Partner;

  @OneToMany((type) => UserJobs, (userJobs) => userJobs.jobId)
  users!: User[];

  constructor() {
    if (!this.uuid) {
      this.uuid = uuid();
    }
  }
}
