import { Column, CreateDateColumn, Entity, ManyToOne, OneToMany, OneToOne, PrimaryColumn, PrimaryGeneratedColumn, UpdateDateColumn} from "typeorm";
import { Job, Stage, User } from ".";

@Entity()
export class UserJobs {

    @PrimaryGeneratedColumn("uuid")
    uuid!: string

    @Column()
    status!: string

    @UpdateDateColumn()
    last_update!: Date

    @CreateDateColumn()
    createdOn!: Date

    @ManyToOne(type => User, user => user.uuid)
    userId!: User

    @ManyToOne(type => Job, job => job.uuid)
    jobId!: Job

    @OneToMany(type => Stage, stage => stage.userJobId, {eager: true})
    stages!: Stage[]

    

}