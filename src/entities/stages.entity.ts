import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from "typeorm";

import { UserJobs } from ".";

@Entity("stages")
export class Stage {
  @PrimaryGeneratedColumn("uuid")
  uuid: string;

  @Column()
  name!: string;

  @Column()
  description!: string;

  @CreateDateColumn()
  created_on!: Date;

  @UpdateDateColumn()
  last_update: Date;

  @Column()
  link: string;

  @Column()
  scheduleDate: Date;

  @Column()
  status: string;

  @ManyToOne((type) => UserJobs, (userJobs) => userJobs.stages, {
    onDelete: "CASCADE",
    cascade: true,
  })
  userJobId!: UserJobs;
}
