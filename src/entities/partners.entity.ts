import bcrypt from "bcrypt";
import { v4 as uuid } from "uuid";
import {
  Entity,
  PrimaryColumn,
  Column,
  BeforeInsert,
  OneToMany,
} from "typeorm";

import { Job } from ".";

@Entity("partners")
export class Partner {
  @PrimaryColumn()
  readonly uuid: string;

  @Column({ nullable: false, unique: true })
  email: string;

  @Column({ nullable: false })
  name: string;

  @Column({ nullable: false })
  password_hash: string;

  @Column({ nullable: false })
  location: string;

  @Column({ nullable: false })
  website: string;

  @BeforeInsert()
  hashPassword() {
    this.password_hash = bcrypt.hashSync(this.password_hash, 10);
  }

  @OneToMany((type) => Job, (job) => job.partnerId)
  jobs!: Job[];

  constructor() {
    if (!this.uuid) {
      this.uuid = uuid();
    }
  }
}
