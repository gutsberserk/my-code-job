import bcrypt from "bcrypt";
import { v4 as uuid } from "uuid";
import { Entity, PrimaryGeneratedColumn, BeforeInsert, Column } from "typeorm";

@Entity("users")
export class User {
  @PrimaryGeneratedColumn("uuid")
  readonly uuid: string;

  @Column({ nullable: false, unique: true })
  email: string;

  @Column({ nullable: false })
  name: string;

  @Column({ nullable: false })
  password_hash: string;

  @Column({ nullable: false })
  birthday: Date;

  @Column({ nullable: false })
  gender: string;

  @Column({ nullable: false })
  cell_number: string;

  @BeforeInsert()
  hashPassword() {
    this.password_hash = bcrypt.hashSync(this.password_hash, 10);
  }

  constructor() {
    if (!this.uuid) {
      this.uuid = uuid();
    }
  }
}
