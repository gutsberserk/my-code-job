import {MigrationInterface, QueryRunner} from "typeorm";

export class updateUserEntity1645675223584 implements MigrationInterface {
    name = 'updateUserEntity1645675223584'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "users" ADD "cell_number" character varying NOT NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "users" DROP COLUMN "cell_number"`);
    }

}
