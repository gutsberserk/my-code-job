import {MigrationInterface, QueryRunner} from "typeorm";

export class alterTables1645102053820 implements MigrationInterface {
    name = 'alterTables1645102053820'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "stages" DROP CONSTRAINT "FK_d880fe7afcecd05156abd25d3dc"`);
        await queryRunner.query(`ALTER TABLE "users" ADD "name" character varying NOT NULL`);
        await queryRunner.query(`ALTER TABLE "user_jobs" DROP CONSTRAINT "FK_5363e9599342fc3fe265d89fab9"`);
        await queryRunner.query(`ALTER TABLE "users" DROP CONSTRAINT "PK_951b8f1dfc94ac1d0301a14b7e1"`);
        await queryRunner.query(`ALTER TABLE "users" DROP COLUMN "uuid"`);
        await queryRunner.query(`ALTER TABLE "users" ADD "uuid" uuid NOT NULL DEFAULT uuid_generate_v4()`);
        await queryRunner.query(`ALTER TABLE "users" ADD CONSTRAINT "PK_951b8f1dfc94ac1d0301a14b7e1" PRIMARY KEY ("uuid")`);
        await queryRunner.query(`ALTER TABLE "user_jobs" DROP COLUMN "userIdUuid"`);
        await queryRunner.query(`ALTER TABLE "user_jobs" ADD "userIdUuid" uuid`);
        await queryRunner.query(`ALTER TABLE "user_jobs" ADD CONSTRAINT "FK_5363e9599342fc3fe265d89fab9" FOREIGN KEY ("userIdUuid") REFERENCES "users"("uuid") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "stages" ADD CONSTRAINT "FK_7245e3039e535ffa1e7786cb490" FOREIGN KEY ("userJobIdUuid") REFERENCES "user_jobs"("uuid") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "stages" DROP CONSTRAINT "FK_7245e3039e535ffa1e7786cb490"`);
        await queryRunner.query(`ALTER TABLE "user_jobs" DROP CONSTRAINT "FK_5363e9599342fc3fe265d89fab9"`);
        await queryRunner.query(`ALTER TABLE "user_jobs" DROP COLUMN "userIdUuid"`);
        await queryRunner.query(`ALTER TABLE "user_jobs" ADD "userIdUuid" character varying`);
        await queryRunner.query(`ALTER TABLE "users" DROP CONSTRAINT "PK_951b8f1dfc94ac1d0301a14b7e1"`);
        await queryRunner.query(`ALTER TABLE "users" DROP COLUMN "uuid"`);
        await queryRunner.query(`ALTER TABLE "users" ADD "uuid" character varying NOT NULL`);
        await queryRunner.query(`ALTER TABLE "users" ADD CONSTRAINT "PK_951b8f1dfc94ac1d0301a14b7e1" PRIMARY KEY ("uuid")`);
        await queryRunner.query(`ALTER TABLE "user_jobs" ADD CONSTRAINT "FK_5363e9599342fc3fe265d89fab9" FOREIGN KEY ("userIdUuid") REFERENCES "users"("uuid") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "users" DROP COLUMN "name"`);
        await queryRunner.query(`ALTER TABLE "stages" ADD CONSTRAINT "FK_d880fe7afcecd05156abd25d3dc" FOREIGN KEY ("userJobIdUuid") REFERENCES "user_jobs"("uuid") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

}
