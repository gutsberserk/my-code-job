import { getRepository } from "typeorm";
import validate from "uuid-validate";

import { Stage } from "../../entities";
import { ErrorHandler } from "../../utils/Error";

class UpdateStageService {
  async execute(stageId: string, data: any) {
    if (!validate(stageId)) {
      throw new ErrorHandler(404, "Stage não encontrado!");
    }

    const stageRepository = getRepository(Stage);
    const stage = await stageRepository.findOne(stageId);

    if (!stage) {
      throw new ErrorHandler(404, "Stage não encontrado!");
    }

    await stageRepository.save({
      ...stage,
      ...data,
    });

    const newStage = await stageRepository.findOne(stageId);

    return newStage;
  }
}

export default UpdateStageService;
