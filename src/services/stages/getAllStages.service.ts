import { getRepository } from "typeorm";
import validate from "uuid-validate";

import { Job, User, UserJobs } from "../../entities";
import { ErrorHandler } from "../../utils/Error";

class ListAllStagesService {
  async execute(userId: string, jobId: string) {
    if (!validate(userId)) {
      throw new ErrorHandler(404, "User não encontrado!");
    }
    if (!validate(jobId)) {
      throw new ErrorHandler(404, "User não encontrado!");
    }

    const userJobRepository = getRepository(UserJobs);
    const userRepository = getRepository(User);
    const jobRepository = getRepository(Job);

    const user = await userRepository.findOne(userId);
    const job = await jobRepository.findOne(jobId);

    const allUserJObs = await userJobRepository.find();

    const userJob = await userJobRepository.findOne({
      where: {
        userId: user,
        jobId: job,
      },
    });

    if (!userJob) {
      throw new ErrorHandler(404, "User não foi registrado neste Job.");
    }

    const stages = userJob.stages;

    return stages;
  }
}

export default ListAllStagesService;
