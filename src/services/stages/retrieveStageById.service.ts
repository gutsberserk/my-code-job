import { getRepository } from "typeorm";
import validate from "uuid-validate";

import { Stage } from "../../entities";
import { ErrorHandler } from "../../utils/Error";

class RetrieveStageByIdService {
  async execute(stageId: string) {
    const stageRepository = getRepository(Stage);

    try {
      if (!validate(stageId)) {
        throw new ErrorHandler(404, "User não encontrado!");
      }
      const stage = await stageRepository.findOne(stageId);

      return stage;
    } catch {
      throw new ErrorHandler(404, "Stage não encontrado!");
    }
  }
}

export default RetrieveStageByIdService;
