import { getRepository } from "typeorm";
import validate from "uuid-validate";

import { Stage } from "../../entities";
import { ErrorHandler } from "../../utils/Error";

class DeleteStageService {
  async execute(stageId: string) {
    if (!validate(stageId)) {
      throw new ErrorHandler(404, "Stage não encontrado!");
    }

    const stageRepository = getRepository(Stage);
    const stage = await stageRepository.findOne(stageId);

    if (!stage) {
      throw new ErrorHandler(404, "Stage não encontrado!");
    }

    await stageRepository.delete(stageId);
  }
}

export default DeleteStageService;
