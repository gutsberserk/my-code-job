import { getRepository } from "typeorm";
import validate from "uuid-validate";

import { Stage, UserJobs } from "../../entities";
import { IStageCreate } from "../../types";
import { ErrorHandler } from "../../utils/Error";

class CreateStageService {
  async execute(jobId: string, userId: string, data: IStageCreate) {
    if (!validate(jobId)) {
      throw new ErrorHandler(404, "Job não encontrado!");
    }
    if (!validate(userId)) {
      throw new ErrorHandler(404, "User não encontrado!");
    }

    const userJobsRepository = getRepository(UserJobs);
    const stageRepository = getRepository(Stage);

    const userJob = await userJobsRepository.findOne({
      where: {
        userId: userId,
        jobId: jobId,
      },
    });

    if (!userJob) {
      throw new ErrorHandler(404, "User não foi registrado neste Job.");
    }

    const stage = stageRepository.create({
      userJobId: userJob,
      name: data.name,
      description: data.description,
      link: data.link,
      status: data.status,
      scheduleDate: data.scheduleDate,
    });

    await stageRepository.save(stage);

    const { userJobId, ...dataStage } = stage;

    const showStage = {
      ...dataStage,
      userJobId: userJobId.uuid,
    };

    return showStage;
  }
}

export default CreateStageService;
