import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import { getCustomRepository, getRepository } from "typeorm";

import { PartnersRepository } from "../../repositories/partners.repository";
import UsersRepository from "../../repositories/users.repository";
import { config } from "../../utils/jwtConfig";
import { ILoginRequest } from "../../types";
import { ErrorHandler, handleError } from "../../utils/Error";

class LoginService {
  async execute({ email, password }: ILoginRequest) {
    const userRepository = getCustomRepository(UsersRepository);
    const partnersRepository = getCustomRepository(PartnersRepository);

    const user = await userRepository.findByEmail(email);

    if (user && bcrypt.compareSync(password, user.password_hash)) {
      const token = jwt.sign({ id: user.uuid }, config.secret, {
        expiresIn: config.expiresIn,
      });

      return token;
    }

    const partner = await partnersRepository.findOne({ email });
    if (partner && bcrypt.compareSync(password, partner.password_hash)) {
      const token = jwt.sign({ id: partner.uuid }, config.secret, {
        expiresIn: config.expiresIn,
      });

      return token;
    }

    if (user === undefined && partner === undefined) {
      throw new ErrorHandler(404, "E-mail não encontrado.");
    }
    if (
      (user && !bcrypt.compareSync(password, user.password_hash)) ||
      (partner && !bcrypt.compareSync(password, partner.password_hash))
    ) {
      throw new ErrorHandler(401, "Password incorreto.");
    }
  }
}

export default LoginService;
