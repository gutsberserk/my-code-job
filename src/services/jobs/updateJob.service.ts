import { getCustomRepository, getRepository } from "typeorm";
import validate from "uuid-validate";

import JobsRepository from "../../repositories/jobs.repository";
import { IJobUpdate } from "../../types";
import { ErrorHandler } from "../../utils/Error";

class UpdateJobService {
  async execute({
    uuid,
    name,
    description,
    plataform,
    location,
    salary,
    benefits,
    format,
    requiredTimeOfXP,
    requiredXP,
    differentialXP,
    hireMode,
    link,
    hasTest,
  }: IJobUpdate) {
    const jobsRepository = getCustomRepository(JobsRepository);

    if (!validate(uuid)) {
      throw new ErrorHandler(404, "Job não encontrado!");
    }

    const uuidRegex =
      /\b[0-9a-f]{8}\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\b[0-9a-f]{12}\b/gm;

    const isRegexValid = uuidRegex.test(uuid);

    if (!isRegexValid) {
      throw new ErrorHandler(400, "Job id não está formatado!");
    }

    const job = await jobsRepository.findOne({ uuid });

    if (!job) {
      throw new ErrorHandler(404, "Job não encontrado!");
    }

    name ? (job.name = name) : job.name;
    description ? (job.description = description) : job.description;
    plataform ? (job.plataform = plataform) : job.plataform;
    location ? (job.location = location) : job.location;
    salary ? (job.salary = salary) : job.salary;
    benefits ? (job.benefits = benefits) : job.benefits;
    format ? (job.format = format) : job.format;
    requiredTimeOfXP
      ? (job.requiredTimeOfXP = requiredTimeOfXP)
      : job.requiredTimeOfXP;
    requiredXP ? (job.requiredXP = requiredXP) : job.requiredXP;
    differentialXP ? (job.differentialXP = differentialXP) : job.differentialXP;
    hireMode ? (job.hireMode = hireMode) : job.hireMode;
    link ? (job.link = link) : job.link;
    hasTest ? (job.hasTest = hasTest) : job.hasTest;

    jobsRepository.save(job);

    return job;
  }
}

export default UpdateJobService;
