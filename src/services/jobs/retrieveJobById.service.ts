import { getCustomRepository } from "typeorm";
import validate from "uuid-validate";

import JobsRepository from "../../repositories/jobs.repository";
import { ErrorHandler } from "../../utils/Error";

class RetrieveJobByIdService {
  async execute(uuid: string) {
    const jobsRepository = getCustomRepository(JobsRepository);

    if (!validate(uuid)) {
      throw new ErrorHandler(404, "Job não encontrado!");
    }

    const uuidRegex =
      /\b[0-9a-f]{8}\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\b[0-9a-f]{12}\b/gm;

    const isRegexValid = uuidRegex.test(uuid);

    if (!isRegexValid) {
      throw new ErrorHandler(400, "Job não está formatado!");
    }
    const job = await jobsRepository.findOne(uuid);

    console.log(job)

    if (!job) {
      throw new ErrorHandler(404, "Job não encontrado!");
    }

    return job;
  }
}

export default RetrieveJobByIdService;
