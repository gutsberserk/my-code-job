import { getRepository } from "typeorm";
import validate from "uuid-validate";

import { Job, User, UserJobs } from "../../entities";
import { ErrorHandler } from "../../utils/Error";

class UnsubscribeJobInUserService {
  async execute(userId: string, jobId: string) {
    if (!validate(jobId)) {
      throw new ErrorHandler(404, "Job não encontrado!");
    }
    if (!validate(userId)) {
      throw new ErrorHandler(404, "User não encontrado!");
    }

    const userJobsRepository = getRepository(UserJobs);
    const userRepository = getRepository(User);
    const jobRepository = getRepository(Job);

    const user = await userRepository.findOne(userId);

    if (!user) {
      throw new ErrorHandler(404, "User não encontrado!");
    }

    const job = await jobRepository.findOne(jobId);

    if (!job) {
      throw new ErrorHandler(404, "Job não encontrado!");
    }

    const userJob = await userJobsRepository.findOne({
      where: {
        userId: user,
        jobId: job,
      },
    });

    if (!userJob) {
      throw new ErrorHandler(
        404,
        "O(a) User não está registrado(a) neste Job."
      );
    }

    await userJobsRepository.delete(userJob.uuid);
  }
}

export default UnsubscribeJobInUserService;
