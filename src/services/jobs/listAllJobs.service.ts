import { getCustomRepository } from "typeorm";

import JobsRepository from "../../repositories/jobs.repository";

class ListAllJobsService {
  async execute() {
    const jobRepository = getCustomRepository(JobsRepository);

    const listOfJobs = await jobRepository.find();

    return listOfJobs;
  }
}

export default ListAllJobsService;
