import { getCustomRepository } from "typeorm";
import validate from "uuid-validate";

import JobsRepository from "../../repositories/jobs.repository";
import { ErrorHandler } from "../../utils/Error";

class DeleteJobService {
  async execute(uuid: string) {
    const jobRepository = getCustomRepository(JobsRepository);

    if (!validate(uuid)) {
      throw new ErrorHandler(404, "Job não encontrado!");
    }

    const uuidRegex =
      /\b[0-9a-f]{8}\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\b[0-9a-f]{12}\b/gm;

    const isRegexValid = uuidRegex.test(uuid);

    if (!isRegexValid) {
      throw new ErrorHandler(400, "Job não está formatado!");
    }

    const job = await jobRepository.findOne(uuid);

    if (!job) {
      throw new ErrorHandler(404, "Job não encontrado!");
    }

    await jobRepository.delete(uuid);

    return "Job removido com sucesso!";
  }
}

export default DeleteJobService;
