import { getRepository } from "typeorm";
import validate from "uuid-validate";

import { Job, User, UserJobs } from "../../entities";
import { IUserJobCreate } from "../../types";
import { ErrorHandler } from "../../utils/Error";

class RegisterJobInService {
  async execute({ userId, jobId }: IUserJobCreate) {
    if (!validate(jobId)) {
      throw new ErrorHandler(404, "Job não encontrado!");
    }
    if (!validate(userId)) {
      throw new ErrorHandler(404, "User não encontrado!");
    }

    const userJobsRepository = getRepository(UserJobs);
    const userRepository = getRepository(User);
    const jobRepository = getRepository(Job);

    const user = await userRepository.findOne(userId);
    const job = await jobRepository.findOne(jobId);

    const userJob = userJobsRepository.create({
      userId: user,
      jobId: job,
      status: "Em andamento",
    });

    userJobsRepository.save(userJob);

    return userJob;
  }
}

export default RegisterJobInService;
