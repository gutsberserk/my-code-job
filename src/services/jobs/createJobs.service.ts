import { getCustomRepository, getRepository } from "typeorm";

import JobsRepository from "../../repositories/jobs.repository";
import { IJobCreate } from "../../types";
import { ErrorHandler } from "../../utils/Error";

class CreateJobService {
  async execute({
    name,
    description,
    plataform,
    location,
    salary,
    benefits,
    format,
    requiredTimeOfXP,
    requiredXP,
    differentialXP,
    hireMode,
    link,
    hasTest,
  }: IJobCreate) {
    const jobsRepository = getCustomRepository(JobsRepository);

    if (!name) {
      throw new ErrorHandler(400, "name é necessário!");
    }

    const job = jobsRepository.create({
      name,
      description,
      plataform,
      location,
      salary,
      benefits,
      format,
      requiredTimeOfXP,
      requiredXP,
      differentialXP,
      hireMode,
      link,
      hasTest,
    });

    await jobsRepository.save(job);

    return job;
  }
}

export default CreateJobService;
