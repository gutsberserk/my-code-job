import { getCustomRepository } from "typeorm";
import UsersRepository from "../../repositories/users.repository";

class ListAllUsersService {
  async execute(page: number = 1) {
    const userRepository = getCustomRepository(UsersRepository);

    const users = userRepository.findPaginated(page);

    return users;
  }
}

export default ListAllUsersService;
