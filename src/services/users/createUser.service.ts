import { getCustomRepository } from "typeorm";

import UsersRepository from "../../repositories/users.repository";
import { IUserCreate } from "../../types";
import { ErrorHandler } from "../../utils/Error";

class CreateUserService {
  async execute({
    name,
    email,
    password,
    cell_number,
    birthday,
    gender,
  }: IUserCreate) {
    const userRepository = getCustomRepository(UsersRepository);

    if (!email) {
      throw new ErrorHandler(400, "É necessário inserir um e-mail válido.");
    }

    const emailAlreadyExists = await userRepository.findOne({ email });

    if (emailAlreadyExists) {
      throw new ErrorHandler(409, "E-mail já registrado.");
    }

    const user = userRepository.create({
      name,
      email,
      password_hash: password,
      cell_number,
      birthday,
      gender,
    });

    await userRepository.save(user);

    return user;
  }
}

export default CreateUserService;
