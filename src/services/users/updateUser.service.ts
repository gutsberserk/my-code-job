import { getCustomRepository } from "typeorm";
import * as bcrypt from "bcryptjs";
import UsersRepository from "../../repositories/users.repository";
import { ErrorHandler } from "../../utils/Error";
import { IUserUpdate } from "../../types";

class UpdateUserService {
  async execute({ uuid, data }: IUserUpdate) {
    const userRepository = getCustomRepository(UsersRepository);

    const user = await userRepository.findOne({ uuid });

    if (user === undefined) {
      throw new ErrorHandler(401, "User não encontrado.");
    }

    if ("password" in data) {
      data.password_hash = await bcrypt.hash(data["password"], 10);
    }
    const { password, ...res } = data;

    await userRepository.update(uuid, res);
    const updateUser = await userRepository.findOne(uuid);
    return updateUser;
  }
}

export default UpdateUserService;
