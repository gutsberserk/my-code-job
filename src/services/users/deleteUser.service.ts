import { getCustomRepository } from "typeorm";

import UsersRepository from "../../repositories/users.repository";
import { ErrorHandler } from "../../utils/Error";

class DeleteUserService {
  async execute(uuid: string) {
    const usersRepository = getCustomRepository(UsersRepository);

    const user = await usersRepository.findOne(uuid);
    if (user === undefined) {
      throw new ErrorHandler(400, "User não encontrado!");
    }

    await usersRepository.delete(uuid);
    return "User removido com sucesso.";
  }
}

export default DeleteUserService;
