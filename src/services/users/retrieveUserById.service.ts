import { getCustomRepository } from "typeorm";
import UsersRepository from "../../repositories/users.repository";
import { ErrorHandler } from "../../utils/Error";

class RetrieveUserByIdService {
  async execute(uuid: string) {
    const userRepository = getCustomRepository(UsersRepository);
    const user = await userRepository.findOne({ uuid });

    if (user === undefined) {
      throw new ErrorHandler(401, "User não encontrado.");
    }

    return user;
  }
}

export default RetrieveUserByIdService;
