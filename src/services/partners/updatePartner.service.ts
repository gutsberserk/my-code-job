import { getCustomRepository } from "typeorm";
import * as bcrypt from "bcryptjs";

import { PartnersRepository } from "../../repositories/partners.repository";
import { IPartnerUpdate } from "../../types";
import { ErrorHandler } from "../../utils/Error";

class UpdatePartnerService {
  async execute({ uuid, data }: IPartnerUpdate) {
    const partnerRepository = getCustomRepository(PartnersRepository);

    const partner = await partnerRepository.findOne({ uuid });

    if (partner === undefined) {
      throw new ErrorHandler(404, "Partner não encontrado.");
    }

    if ("password" in data) {
      data.password_hash = await bcrypt.hash(data["password"], 10);
    }
    const { password, ...res } = data;

    await partnerRepository.update(uuid, res);
    const updateUser = await partnerRepository.findOne(uuid);
    return updateUser;
  }
}

export default UpdatePartnerService;
