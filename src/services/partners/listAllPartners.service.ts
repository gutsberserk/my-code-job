import { getCustomRepository } from "typeorm";

import { PartnersRepository } from "../../repositories/partners.repository";

class ListAllPartnersService {
  async execute() {
    const partnerRepository = getCustomRepository(PartnersRepository);

    const partner = partnerRepository.find();

    return partner;
  }
}

export default ListAllPartnersService;
