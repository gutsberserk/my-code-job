import { getCustomRepository } from "typeorm";

import JobsRepository from "../../repositories/jobs.repository";

class ListAllJobsPartnersService {
  async execute(uuid: string) {
    const jobsRepository = getCustomRepository(JobsRepository);

    const jobs = await jobsRepository.query(
      `SELECT * 
      FROM "jobs"
      WHERE "jobs"."partnerIdUuid" = $1 `,
      [uuid]
    );

    return jobs;
  }
}

export default ListAllJobsPartnersService;
