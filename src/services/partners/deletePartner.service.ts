import { getCustomRepository } from "typeorm";

import { PartnersRepository } from "../../repositories/partners.repository";
import { ErrorHandler } from "../../utils/Error";

class DeletePartnerService {
  async execute(uuid: string) {
    const partnersRepository = getCustomRepository(PartnersRepository);

    const partner = await partnersRepository.findOne(uuid);
    if (partner === undefined) {
      throw new ErrorHandler(404, "Partner não encontrado.");
    }

    await partnersRepository.delete(uuid);
    return "Partner removido com sucesso!";
  }
}

export default DeletePartnerService;
