import { getCustomRepository } from "typeorm";
import { ErrorHandler } from "../../utils/Error";

import { PartnersRepository } from "../../repositories/partners.repository";
import UsersRepository from "../../repositories/users.repository";
import { IPartnerCreate } from "../../types";

class CreatePartnerService {
  async execute({ name, email, password, location, website }: IPartnerCreate) {
    const partnerRepository = getCustomRepository(PartnersRepository);
    const userRepository = getCustomRepository(UsersRepository);

    if (!email) {
      throw new ErrorHandler(400, "É necessário inserir um e-mail válido.");
    }

    const emailAlreadyExistsInPartners = await partnerRepository.findOne({
      email,
    });
    const emailAlreadyExists = await userRepository.findOne({ email });

    if (emailAlreadyExists || emailAlreadyExistsInPartners) {
      throw new ErrorHandler(409, "E-mail já registrado.");
    }

    const partner = partnerRepository.create({
      name,
      email,
      password_hash: password,
      location,
      website,
    });

    await partnerRepository.save(partner);

    return partner;
  }
}

export default CreatePartnerService;
