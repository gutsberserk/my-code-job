import { getCustomRepository } from "typeorm";

import { PartnersRepository } from "../../repositories/partners.repository";
import { ErrorHandler } from "../../utils/Error";

class RetrievePartnerByIdService {
  async execute(uuid: string) {
    const partnerRepository = getCustomRepository(PartnersRepository);
    const partner = await partnerRepository.findOne({ uuid });

    if (partner === undefined) {
      throw new ErrorHandler(404, "Partner não encontrado.");
    }

    return partner;
  }
}

export default RetrievePartnerByIdService;
