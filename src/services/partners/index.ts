import CreatePartnerService from "./createPartner.service";
import DeletePartnerService from "./deletePartner.service";
import ListAllPartnersService from "./listAllPartners.service";
import RetrievePartnerByIdService from "./retrievePartnerById.service";
import UpdatePartnerService from "./updatePartner.service";
import ListAllJobsPartnersService from "./listAllJobsPartner.service";

export {
  CreatePartnerService,
  DeletePartnerService,
  ListAllPartnersService,
  RetrievePartnerByIdService,
  UpdatePartnerService,
  ListAllJobsPartnersService,
};
