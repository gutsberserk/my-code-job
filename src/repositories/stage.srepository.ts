import { EntityRepository, Repository } from "typeorm";
import { Stage } from "../entities";

@EntityRepository(Stage)
class StageRepository extends Repository<Stage>{
}

export default StageRepository