import { EntityRepository, Repository } from "typeorm";
import { Job } from "../entities/jobs.entity";

@EntityRepository(Job)
class JobsRepository extends Repository<Job> {}

export default JobsRepository;
