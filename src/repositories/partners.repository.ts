import { EntityRepository, Repository } from "typeorm";
import { Partner } from "../entities/index";

@EntityRepository(Partner)
class PartnersRepository extends Repository<Partner> {}

export { PartnersRepository };
