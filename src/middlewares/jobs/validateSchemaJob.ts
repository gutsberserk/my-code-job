import { Request, Response, NextFunction } from "express";
import { SchemaOf } from "yup";

import { IJobCreate } from "../../types";

declare global {
  namespace Express {
    interface Request {
      newJob: IJobCreate;
    }
  }
}

export const validateSchemaCreateJob =
  (schema: SchemaOf<IJobCreate>) =>
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      const data = req.body;

      try {
        const validatedData = await schema.validate(data, {
          abortEarly: false,
          stripUnknown: true,
        });

        req.newJob = validatedData;

        next();
      } catch (err: any) {
        return res.status(400).json({
          error: err.errors?.join(", "),
        });
      }
    } catch (err) {
      next(err);
    }
  };
