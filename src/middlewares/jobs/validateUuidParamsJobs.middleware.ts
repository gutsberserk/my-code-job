import { NextFunction, Request, Response } from "express";
import validate from "uuid-validate";

import { ErrorHandler } from "../../utils/Error";

export const validateUuidParamsJobs = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const { uuid } = req.params;

  if (!validate(uuid)) {
    next(new ErrorHandler(404, "Job não encontrado!"));
  }

  next();
};
