import { NextFunction, Request, Response } from "express";
import { getRepository } from "typeorm";
import validate from "uuid-validate";

import { Job } from "../../entities";
import { ErrorHandler } from "../../utils/Error";

export const verifyJobExists = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const jobId = req.params.uuid;

  if (!validate(jobId)) {
    next(new ErrorHandler(404, "Job não encontrado!"));
  }
  const jobRepository = getRepository(Job);

  const job = await jobRepository.findOne(jobId);

  if (!job) {
    next(new ErrorHandler(404, "Job não encontrado!"));
  }

  next();
};
