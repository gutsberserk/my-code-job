import { NextFunction, Request, Response } from "express";
import { ErrorHandler } from "../../utils/Error";
import validate from "uuid-validate";

export const validateUuidParamsStages = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const { uuid } = req.params;

  if (!validate(uuid)) {
    next(new ErrorHandler(404, "Stage não encontrado!"));
  }

  next();
};
