import { NextFunction, Request, Response } from "express";
import { getRepository } from "typeorm";
import validate from "uuid-validate";

import { Stage, UserJobs } from "../../entities";
import { ErrorHandler } from "../../utils/Error";

export const verifyStageBelongUser = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const userId = req.userId;
  const stageId = req.params.uuid;

  if (!validate(userId)) {
    next(new ErrorHandler(404, "User não encontrado!"));
  }

  if (!validate(stageId)) {
    next(new ErrorHandler(404, "Stage não encontrado!"));
  }

  const userJobRepository = getRepository(UserJobs);
  const stageRepository = getRepository(Stage);

  const stage = await stageRepository.findOne(stageId);

  if (!stage) {
    next(new ErrorHandler(404, "Stage não enontrado!"));
  }

  const userIdBelong = await userJobRepository.query(
    `
    SELECT "users"."uuid"
    FROM "user_jobs"
    JOIN "stages" ON "stages"."userJobIdUuid" = "user_jobs"."uuid"
    JOIN "users" ON "users"."uuid" = "user_jobs"."userIdUuid"
    WHERE "stages"."uuid" = $1 ;
    `,
    [stageId]
  );

  if (userIdBelong.lenght == 0) {
    next(
      new ErrorHandler(
        401,
        "Nenhuma operação pode ser realizada com este Stage já que não pertence ao User."
      )
    );
  }

  if (userId != userIdBelong[0].uuid) {
    next(
      new ErrorHandler(
        401,
        "Nenhuma operação pode ser realizada com este Stage já que não pertence ao User."
      )
    );
  }

  next();
};
