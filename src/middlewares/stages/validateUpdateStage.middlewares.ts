import { NextFunction, Request, Response } from "express";

import { ErrorHandler } from "../../utils/Error";

export const validateUpdateStage = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const data = req.body;
  const availableKeys = ["name", "description", "status", "link"];

  const dataKeys = Object.keys(data);
  for (let i = 0; i < dataKeys.length; i++) {
    if (availableKeys.includes(dataKeys[i]) == false) {
      next(
        new ErrorHandler(
          422,
          "Apenas as keys: " + availableKeys.join(", ") + " são disponíveis."
        )
      );
    }
  }

  next();
};
