import { Request, Response, NextFunction } from "express";

export const validatePartnerUpdate = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const { email, uuid, ...data } = req.body;
  if (email || uuid) {
    res.status(401).json({
      message: "E-mail e uuid são atributos que não podem ser alterados.",
    });
  } else {
    next();
  }
};
