import { Request, Response, NextFunction } from "express";

export const validatePartnerKeys = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  for (let key in req.body) {
    if (typeof req.body[key] !== "string") {
      res.status(400).json({
        message: "Todas as keys precisam ser string.",
      });
    }
  }
  next();
};
