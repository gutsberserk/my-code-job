import { Request, Response, NextFunction } from "express";
import { SchemaOf } from "yup";
import { IPartnerCreate } from "../../types";

declare global {
  namespace Express {
    interface Request {
      newPartner: IPartnerCreate;
    }
  }
}

export const validateSchemaPartner =
  (schema: SchemaOf<IPartnerCreate>) =>
  async (request: Request, response: Response, next: NextFunction) => {
    try {
      const data = request.body;

      try {
        const validatedData = await schema.validate(data, {
          abortEarly: false,
          stripUnknown: true,
        });

        request.newPartner = validatedData;

        next();
      } catch (err: any) {
        return response.status(400).json({
          error: err.errors?.join(", "),
        });
      }
    } catch (err) {
      next(err);
    }
  };
