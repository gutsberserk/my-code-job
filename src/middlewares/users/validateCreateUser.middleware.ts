import { NextFunction, Request, Response } from "express";

import { ErrorHandler } from "../../utils/Error";

export const validateCreateUser = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const data = req.body;
  const requiredStage = [
    "name",
    "email",
    "password",
    "birthday",
    "cell_number",
    "gender",
  ];

  const missingKeys = [];
  for (let i = 0; i < requiredStage.length; i++) {
    if (Object.keys(data).includes(requiredStage[i]) == false) {
      missingKeys.push(requiredStage[i]);
    }
  }

  if (missingKeys.length > 0) {
    next(
      new ErrorHandler(
        400,
        "As keys: " + missingKeys.join(", ") + " são necessárias."
      )
    );
  }

  next();
};
