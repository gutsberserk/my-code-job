import { Request, Response, NextFunction } from "express";
import { SchemaOf } from "yup";

import { ILoginRequest } from "../../types";

declare global {
  namespace Express {
    interface Request {
      login: ILoginRequest;
    }
  }
}

export const validateSchemaLogin =
  (schema: SchemaOf<ILoginRequest>) =>
  async (request: Request, response: Response, next: NextFunction) => {
    try {
      const data = request.body;

      try {
        const validatedData = await schema.validate(data, {
          abortEarly: false,
          stripUnknown: true,
        });

        request.login = validatedData;

        next();
      } catch (err: any) {
        return response.status(400).json({
          error: err.errors?.join(", "),
        });
      }
    } catch (err) {
      next(err);
    }
  };
