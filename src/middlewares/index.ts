import { isLogged } from "./auth/authentication.middleware";
import { isOwner } from "./auth/isOwner.middleware";

import { validateCreateJob } from "./jobs/validateCreateJob.middlewares";
import { validateSchemaCreateJob } from "./jobs/validateSchemaJob";
import { validateUuidParamsJobs } from "./jobs/validateUuidParamsJobs.middleware";
import { verifyJobExists } from "./jobs/verifyJobExists.middlewares";

import { validatePartnerKeys } from "./partners/validateCreatePartner.middleware";
import { validatePartnerUpdate } from "./partners/validateUpdatePartner";
import { validateUuidParamsPartners } from "./partners/validateUuidParamsPartners.middleware";

import { validateCreateStage } from "./stages/validateCreateStage.middlewares";
import { validateUpdateStage } from "./stages/validateUpdateStage.middlewares";
import { validateUuidParamsStages } from "./stages/validateUuidParamsStages.middleware";
import { verifyStageBelongUser } from "./stages/verifyStageBelongUser.middleware";

import { validateCreateUser } from "./users/validateCreateUser.middleware";
import { validateUserUpdate } from "./users/validateUpdateUser.middleware";
import { validateUuidParamsUsers } from "./users/validateUuidParamsUsers.middleware";

import { validateSchemaLogin } from "./login/validateSchemaLogin.middleware";

export {
  isLogged,
  isOwner,
  validateCreateJob,
  validateSchemaCreateJob,
  validateUuidParamsJobs,
  verifyJobExists,
  validatePartnerKeys,
  validatePartnerUpdate,
  validateUuidParamsPartners,
  validateCreateStage,
  validateUpdateStage,
  validateUuidParamsStages,
  verifyStageBelongUser,
  validateCreateUser,
  validateUserUpdate,
  validateUuidParamsUsers,
  validateSchemaLogin,
};
