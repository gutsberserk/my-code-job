import { Request, Response, NextFunction } from "express";
import jwt from "jsonwebtoken";

import { config } from "../../utils/jwtConfig";

declare global {
  namespace Express {
    interface Request {
      userId: string;
    }
  }
}

export const isLogged = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const headers = req.headers.authorization;
    if (!headers) {
      throw new Error("Missing Authorization headers");
    }

    const token = req.headers.authorization?.split(" ")[1] || "";
    jwt.verify(token, config.secret, (err: any, decoded: any) => {
      if (err) {
        throw new Error("Token inválido.");
      }
      if (decoded.id) {
        req.userId = decoded.id;
        next();
      } else {
        throw new Error(
          "Você precisa possuir este item para poder realizar uma ação!"
        );
      }
    });
  } catch (error) {
    return res.status(401).json({ message: error.message });
  }
};
