// Users

export interface IUserCreate {
  name: string;
  email: string;
  password: string;
  cell_number: string;
  birthday: string;
  gender: string;
}

export interface IUserUpdate {
  uuid: string;
  data: any;
}

// Login

export interface ILoginRequest {
  email: string;
  password: string;
}

// Partners

export interface IPartnerCreate {
  name: string;
  email: string;
  password: string;
  location: string;
  website: string;
}

export interface IPartnerUpdate {
  uuid: string;
  data: any;
}

// Jobs

export interface IJobCreate {
  name: string;
  description: string;
  plataform?: string;
  location: string;
  salary?: string;
  benefits?: string;
  format: string;
  requiredTimeOfXP?: string;
  requiredXP?: string;
  differentialXP?: string;
  hireMode: string;
  link: string;
  hasTest?: boolean;
}

export interface IJob {
  name: string;
  description: string;
  location: string;
  salary: string;
  benefits: string;
  format: string;
  requiredTimeOfXP: string;
  requiredXP: string;
  differentialXP: string;
  hireMode: string;
  link: string;
  hasTest: boolean;
}

export interface IUserJobCreate {
  userId: string;
  jobId: string;
}

export interface IJobUpdate {
  uuid: string;
  name?: string;
  description?: string;
  plataform?: string;
  location?: string;
  salary?: string;
  benefits?: string;
  format?: string;
  requiredTimeOfXP?: string;
  requiredXP?: string;
  differentialXP?: string;
  hireMode?: string;
  link?: string;
  hasTest?: boolean;
}

// Stages

export interface IStageCreate {
  name: string;
  description: string;
  link: string;
  status: string;
  scheduleDate: string;
}
