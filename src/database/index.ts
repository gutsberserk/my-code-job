import { ConnectionOptions } from "typeorm";
import dotenv from "dotenv";

dotenv.config();

const config: ConnectionOptions = {
  type: "postgres",
  url: process.env.DATABASE_URL,
  database: process.env.NODE_ENV === "test" ? "test_crud_node" : "crud_node",
  synchronize: true,
  logging: false,
  cli: {
    entitiesDir: "src/entities",
    migrationsDir: "src/migrations",
  },
  ssl:
    process.env.NODE_ENV === "production"
      ? { rejectUnauthorized: false }
      : false,
  entities:
    process.env.NODE_ENV === "production"
      ? ["dist/entities/**/*.js"]
      : ["src/entities/**/*.ts"],
  migrations:
    process.env.NODE_ENV === "production"
      ? ["dist/migrations/**/*.js"]
      : ["src/migrations/**/*.ts"],
};

export default config;
