import express, { Express } from "express";
import { docRoute } from "./doc.route";
import { jobRoute } from "./job.route";
import { loginRoute } from "./login.route";
import { partnerRoute } from "./partner.route";
import { stageRoute } from "./stage.route";
import { userRoute } from "./user.route";

export const routesApp = (app: Express) => {
  app.use(express.json());
  loginRoute(app);
  userRoute(app);
  partnerRoute(app);
  jobRoute(app);
  stageRoute(app);
  loginRoute(app);
  docRoute(app);
};
