import { Router, Express } from "express";
import { GetUserJobsController } from "../controllers/jobs/getUserJobs.controller";

import {
  isLogged,
  validateCreateJob,
  validateUuidParamsJobs,
  verifyJobExists,
} from "../middlewares";

import {
  CreateJobController,
  ListJobByIdController,
  UpdateJobController,
  DeleteJobController,
} from "../controllers/jobs";
import ListAllJobsController from "../controllers/jobs/listAllJobs.controller";
import RegisterJobInUserController from "../controllers/jobs/registerJobInUser.controller";
import UnsubscribeJobInUserController from "../controllers/jobs/unsubscribeJobInUser.controller";
import { validateSchemaCreateJob } from "../middlewares/jobs/validateSchemaJob";
import jobCreateSchema from "../schemas/JobSchema";

const route = Router();

const createJobController = new CreateJobController();
const listAllJobsController = new ListAllJobsController();
const retrieveJobByIdController = new ListJobByIdController();
const updateJobController = new UpdateJobController();
const deleteJobController = new DeleteJobController();
const registerJobInUserController = new RegisterJobInUserController();
const unsubscribeJobInUserController = new UnsubscribeJobInUserController();

export const jobRoute = (app: Express) => {
  route.post(
    "",
    validateCreateJob,
    validateSchemaCreateJob(jobCreateSchema),
    isLogged,
    createJobController.handle
  );
  route.get("", isLogged, listAllJobsController.handle);
  route.get(
    "/:uuid",
    validateUuidParamsJobs,
    isLogged,
    verifyJobExists,
    retrieveJobByIdController.handle
  );
  route.patch(
    "/:uuid",
    validateUuidParamsJobs,
    isLogged,
    verifyJobExists,
    updateJobController.handle
  );
  route.delete(
    "/:uuid",
    validateUuidParamsJobs,
    isLogged,
    verifyJobExists,
    deleteJobController.handle
  );

  route.post(
    "/register/:uuid",
    validateUuidParamsJobs,
    isLogged,
    registerJobInUserController.handle
  );
  route.delete(
    "/register/:uuid",
    validateUuidParamsJobs,
    isLogged,
    unsubscribeJobInUserController.handle
  );

  route.get("/user/jobs", isLogged, GetUserJobsController);

  app.use("/job", route);
};
