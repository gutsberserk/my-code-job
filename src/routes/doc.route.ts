import { Router, Express } from "express";
import swaggerUiExpress from "swagger-ui-express";

import swaggerDocument from "../swagger.json";

const route = Router();

export const docRoute = (app: Express) => {
  route.use(
    "/api-documentation",
    swaggerUiExpress.serve,
    swaggerUiExpress.setup(swaggerDocument)
  );
  app.use("", route);
};
