import { Router, Express } from "express";
import { validatePartnerUpdate } from "../middlewares/partners/validateUpdatePartner";
import { validatePartnerKeys } from "../middlewares/partners/validateCreatePartner.middleware";
import { validateUuidParamsPartners } from "../middlewares/partners/validateUuidParamsPartners.middleware";

const route = Router();

import {
  CreatePartnerController,
  RetrievePartnerByIdController,
  ListAllPartnersController,
  UpdatePartnerController,
  DeletePartnerController,
  ListAllJobsPartnersController,
} from "../controllers/partners/index";
import { isLogged } from "../middlewares/";
import { isOwner } from "../middlewares/";
import { validateSchemaPartner } from "../middlewares/partners/validateSchemaLogin.middleware";
import PartnerSchema from "../schemas/PartnerSchema";

const createPartnerController = new CreatePartnerController();
const listAllPartnersController = new ListAllPartnersController();
const retrievePartnerByIdController = new RetrievePartnerByIdController();
const updatePartnerController = new UpdatePartnerController();
const deletePartnerController = new DeletePartnerController();
const listAllJobsPartnersController = new ListAllJobsPartnersController();

export const partnerRoute = (app: Express) => {
  route.post(
    "",
    validatePartnerKeys,
    validateSchemaPartner(PartnerSchema),
    createPartnerController.handle
  );
  route.get("", isLogged, listAllPartnersController.handle);
  route.get(
    "/:uuid",
    validateUuidParamsPartners,
    isLogged,
    retrievePartnerByIdController.handle
  );
  route.patch(
    "/:uuid",
    isLogged,
    isOwner,
    validateUuidParamsPartners,
    validatePartnerUpdate,
    validatePartnerKeys,
    updatePartnerController.handle
  );
  route.delete(
    "/:uuid",
    validateUuidParamsPartners,
    isLogged,
    isOwner,
    deletePartnerController.handle
  );
  route.get(
    "/:uuid/jobs",
    validateUuidParamsPartners,
    isLogged,
    listAllJobsPartnersController.handle
  );
  app.use("/partner", route);
};
