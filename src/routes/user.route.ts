import { Router, Express } from "express";
import {
  CreateUserController,
  ListAllUsersController,
  UpdateUserController,
  DeleteUserController,
  RetrieveUserByIdController,
} from "../controllers/users";
import { validateCreateUser } from "../middlewares/users/validateCreateUser.middleware";
import { validateUserUpdate } from "../middlewares/users/validateUpdateUser.middleware";

import { isLogged, validateUuidParamsUsers } from "../middlewares";

const route = Router();

const createUserController = new CreateUserController();
const retrieveUserByIdController = new RetrieveUserByIdController();
const listAllUserController = new ListAllUsersController();
const updateUserController = new UpdateUserController();
const deleteUserController = new DeleteUserController();

export const userRoute = (app: Express) => {
  route.post("", validateCreateUser, createUserController.handle);
  route.get(
    "/:uuid",
    validateUuidParamsUsers,
    isLogged,
    retrieveUserByIdController.handle
  );
  route.get("", isLogged, listAllUserController.handle);
  route.patch(
    "/:uuid",
    validateUuidParamsUsers,
    validateUserUpdate,
    isLogged,
    updateUserController.handle
  );
  route.delete(
    "/:uuid",
    validateUuidParamsUsers,
    isLogged,
    deleteUserController.handle
  );
  app.use("/user", route);
};
