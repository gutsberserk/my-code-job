import { Router, Express } from "express";
import LoginController from "../controllers/login/login.controller";
import { validateSchemaLogin } from "../middlewares";

import LoginSchema from "../schemas/LoginSchema";

const route = Router();

const loginController = new LoginController();

export const loginRoute = (app: Express) => {
  route.post("", validateSchemaLogin(LoginSchema), loginController.handle);
  app.use("/login", route);
};
