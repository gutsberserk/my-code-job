import { Router, Express } from "express";
import {
  CreateStageController,
  RetrieveStageByIdController,
  ListAllStagesController,
  DeleteStageController,
} from "../controllers/stages";
import UpdateStageController from "../controllers/stages/updateStage.controllers";
import {
  validateCreateStage,
  isLogged,
  validateUpdateStage,
  verifyJobExists,
  verifyStageBelongUser,
} from "../middlewares";

const route = Router();

const createStageController = new CreateStageController();
const updateStageController = new UpdateStageController();
const retrieveStageByIdController = new RetrieveStageByIdController();
const listAllStagesController = new ListAllStagesController();
const deleteStageController = new DeleteStageController();

export const stageRoute = (app: Express) => {
  route.post(
    "/job/:uuid",
    isLogged,
    verifyJobExists,
    validateCreateStage,
    createStageController.handle
  );
  route.patch("/:uuid",isLogged, verifyStageBelongUser, validateUpdateStage, updateStageController.handle);
  route.get("/:uuid", isLogged, verifyStageBelongUser, retrieveStageByIdController.handle);
  route.get(
    "/job/:uuid",
    isLogged,
    verifyJobExists,
    listAllStagesController.handle
  );
  route.delete(
    "/:uuid",
    isLogged,
    verifyStageBelongUser,
    deleteStageController.handle
  );
  app.use("/stage", route);
};
