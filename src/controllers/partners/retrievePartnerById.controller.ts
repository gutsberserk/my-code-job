import { NextFunction, Request, Response } from "express";

import { RetrievePartnerByIdService } from "../../services/partners";

class RetrievePartnerByIdController {
  async handle(request: Request, response: Response, next: NextFunction) {
    const listPartnerByIdService = new RetrievePartnerByIdService();

    const { uuid } = request.params;

    try {
      const partner = await listPartnerByIdService.execute(uuid);
      const { password_hash, ...data } = partner;
      return response.json(data);
    } catch (err) {
      next(err);
    }
  }
}

export default RetrievePartnerByIdController;
