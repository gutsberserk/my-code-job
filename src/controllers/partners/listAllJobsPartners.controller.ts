import { Request, Response } from "express";

import { ListAllJobsPartnersService } from "../../services/partners";

class ListAllJobsPartnersController {
  async handle(request: Request, response: Response) {
    const listAllJobsPartnersService = new ListAllJobsPartnersService();

    const { uuid } = request.params;
    const res = [];

    const jobs = await listAllJobsPartnersService.execute(uuid);
    for (let job in jobs) {
      const { password_hash, partnerIdUuid, ...data } = jobs[job];
      res.push(data);
    }

    return response.json(res);
  }
}

export default ListAllJobsPartnersController;
