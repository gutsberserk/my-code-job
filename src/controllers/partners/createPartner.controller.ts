import { NextFunction, Request, Response } from "express";

import { CreatePartnerService } from "../../services/partners";

class CreatePartnerController {
  async handle(request: Request, response: Response, next: NextFunction) {
    try {
      const { name, email, location, password, website } = request.body;

      const createPartnerService = new CreatePartnerService();

      const partner = await createPartnerService.execute({
        name,
        email,
        password,
        location,
        website,
      });
      const { password_hash, ...data } = partner;

      return response.status(201).json(data);
    } catch (err) {
      next(err);
    }
  }
}

export default CreatePartnerController;
