import { NextFunction, Request, Response } from "express";
import { UpdatePartnerService } from "../../services/partners";

class UpdatePartnerController {
  async handle(request: Request, response: Response, next: NextFunction) {
    try {
      const { uuid } = request.params;
      const data = request.body;

      const updatePartnerService = new UpdatePartnerService();

      const partner = await updatePartnerService.execute({ uuid, data });

      const { password_hash, ...res }: any = partner;

      return response.json(res);
    } catch (err) {
      next(err);
    }
  }
}

export default UpdatePartnerController;
