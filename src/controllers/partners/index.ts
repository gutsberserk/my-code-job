import CreatePartnerController from "./createPartner.controller";
import DeletePartnerController from "./deletePartner.controller";
import ListAllPartnersController from "./listAllPartners.controller";
import UpdatePartnerController from "./updatePartner.controller";
import RetrievePartnerByIdController from "./retrievePartnerById.controller";
import ListAllJobsPartnersController from "./listAllJobsPartners.controller";

export {
  CreatePartnerController,
  DeletePartnerController,
  ListAllPartnersController,
  UpdatePartnerController,
  RetrievePartnerByIdController,
  ListAllJobsPartnersController,
};
