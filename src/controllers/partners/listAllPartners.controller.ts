import { Request, Response } from "express";

import { ListAllPartnersService } from "../../services/partners";

class ListAllPartnersController {
  async handle(request: Request, response: Response) {
    const listAllPartnersService = new ListAllPartnersService();

    const resp = [];

    const partners = await listAllPartnersService.execute();
    for (let i in partners) {
      const { password_hash, ...data } = partners[i];
      resp.push(data);
    }

    return response.json(resp);
  }
}

export default ListAllPartnersController;
