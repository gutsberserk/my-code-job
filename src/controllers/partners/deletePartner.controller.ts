import { NextFunction, Request, Response } from "express";

import { DeletePartnerService } from "../../services/partners";

class DeletePartnerController {
  async handle(request: Request, response: Response, next: NextFunction) {
    try {
      const { uuid } = request.params;

      const deletePartnerService = new DeletePartnerService();

      const partner = await deletePartnerService.execute(uuid);

      return response
        .status(200)
        .json({ message: "Partner removido com sucesso!" });
    } catch (err) {
      next(err);
    }
  }
}

export default DeletePartnerController;
