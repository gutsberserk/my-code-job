import { NextFunction, Request, Response } from "express";

import { RegisterJobInUserService } from "../../services/jobs";
import { ErrorHandler, handleError } from "../../utils/Error";

class RegisterJobInUserController {
  async handle(request: Request, response: Response, next: NextFunction) {
    try {
      const jobId = request.params.uuid;
      const userId = request.userId;

      const registerInJobService = new RegisterJobInUserService();

      const userJob = await registerInJobService.execute({ userId, jobId });

      response.send(userJob);
    } catch (err) {
      if (err instanceof ErrorHandler) {
        handleError(err, response);
      }
    }
  }
}

export default RegisterJobInUserController;
