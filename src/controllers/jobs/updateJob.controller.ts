import { Request, Response } from "express";

import { UpdateJobService } from "../../services/jobs";
import { ErrorHandler, handleError } from "../../utils/Error";

class UpdateJobController {
  async handle(request: Request, response: Response) {
    const { uuid } = request.params;

    const data = request.body;

    const updateJobService = new UpdateJobService();

    try {
      const updateJob = await updateJobService.execute({ uuid, ...data });

      return response.json(updateJob);
    } catch (err) {
      if (err instanceof ErrorHandler) {
        handleError(err, response);
      }
    }
  }
}

export default UpdateJobController;
