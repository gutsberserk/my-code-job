import { NextFunction, Request, Response } from "express";
import { getRepository } from "typeorm";

import { UserJobs } from "../../entities";
import { ErrorHandler, handleError } from "../../utils/Error";

export const GetUserJobsController = async (
  request: Request,
  response: Response,
  next: NextFunction
) => {
  const id = request.userId;

  try {
    const userJobsRepository = getRepository(UserJobs);

    const userJob = await userJobsRepository.find({
      where: {
        userId: id,
      },
    });

    response.send({ data: userJob });
  } catch (err) {
    if (err instanceof ErrorHandler) {
      handleError(err, response);
    }
  }
};
