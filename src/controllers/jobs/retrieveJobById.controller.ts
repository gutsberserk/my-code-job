import { Request, Response } from "express";

import { RetrieveJobByIdService } from "../../services/jobs";
import { ErrorHandler, handleError } from "../../utils/Error";

class RetrieveJobByIdController {
  async handle(request: Request, response: Response) {
    const { uuid } = request.params;

    const jobById = new RetrieveJobByIdService();

    try {
      const job = await jobById.execute(uuid);

      return response.json(job);
    } catch (err) {
      if (err instanceof ErrorHandler) {
        handleError(err, response);
      }
    }
  }
}

export default RetrieveJobByIdController;
