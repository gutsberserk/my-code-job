import { Request, Response } from "express";

import { ListAllJobsService } from "../../services/jobs";
import { IJob } from "../../types";

class ListAllJobsController {
  async handle(request: Request, response: Response) {
    const job = new ListAllJobsService();

    const jobList: IJob[] = await job.execute();

    return response.json(jobList);
  }
}

export default ListAllJobsController;
