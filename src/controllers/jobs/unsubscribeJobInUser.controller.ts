import { NextFunction, Request, Response } from "express";

import { UnsubscribeJobInUserService } from "../../services/jobs";
import { ErrorHandler, handleError } from "../../utils/Error";

class UnsubscribeJobInUserController {
  async handle(request: Request, response: Response, next: NextFunction) {
    try {
      const jobId = request.params.uuid;
      const userId = request.userId;

      const unsubscribeJobService = new UnsubscribeJobInUserService();

      await unsubscribeJobService.execute(userId, jobId);

      response.send({ data: "Desinscrição completa!" });
    } catch (err) {
      if (err instanceof ErrorHandler) {
        handleError(err, response);
      }
    }
  }
}

export default UnsubscribeJobInUserController;
