import { Request, Response } from "express";

import { IJobCreate } from "../../types";
import JobCreateService from "../../services/jobs/createJobs.service";
import { ErrorHandler, handleError } from "../../utils/Error";

class CreateJobController {
  async handle(request: Request, response: Response) {
    try {
      const {
        name,
        description,
        plataform,
        location,
        salary,
        benefits,
        format,
        requiredTimeOfXP,
        requiredXP,
        differentialXP,
        hireMode,
        link,
        hasTest,
      } = request.body;

      const createJobService = new JobCreateService();

      const job: IJobCreate = await createJobService.execute({
        name,
        description,
        plataform,
        location,
        salary,
        benefits,
        format,
        requiredTimeOfXP,
        requiredXP,
        differentialXP,
        hireMode,
        link,
        hasTest,
      });

      return response.status(201).json(job);
    } catch (err) {
      if (err instanceof ErrorHandler) {
        handleError(err, response);
      }
    }
  }
}

export default CreateJobController;
