import { Request, Response } from "express";

import { DeleteJobService } from "../../services/jobs";
import { ErrorHandler, handleError } from "../../utils/Error";

class DeleteJobController {
  async handle(request: Request, response: Response) {
    const { uuid } = request.params;

    try {
      const jobToBeDeleted = new DeleteJobService();

      const deleted = await jobToBeDeleted.execute(uuid);

      return response
        .status(200)
        .json({ message: "Job removido com sucesso!" });
    } catch (err) {
      if (err instanceof ErrorHandler) {
        handleError(err, response);
      }
    }
  }
}

export default DeleteJobController;
