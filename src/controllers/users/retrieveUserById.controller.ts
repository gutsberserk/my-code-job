import { NextFunction, Request, Response } from "express";

import { RetrieveUserByIdService } from "../../services/users";

class RetrieveUserByIdController {
  async handle(request: Request, response: Response, next: NextFunction) {
    const listUserByIdService = new RetrieveUserByIdService();

    const { uuid } = request.params;

    try {
      const partner = await listUserByIdService.execute(uuid);

      const { password_hash, ...data } = partner;

      return response.json(data);
    } catch (err) {
      next(err);
    }
  }
}

export default RetrieveUserByIdController;
