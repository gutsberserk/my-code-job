import { Request, Response } from "express";

import { ListAllUsersService } from "../../services/users";

class ListAllUsersController {
  async handle(request: Request, response: Response) {
    const listAllUsersService = new ListAllUsersService();

    const resp = [];

    const pageQuery = request.query.page
      ? parseInt(request.query.page as string)
      : 1;
    const users = await listAllUsersService.execute(pageQuery);

    for (let i in users) {
      const { password_hash, ...data } = users[i];
      resp.push(data);
    }

    return response.json(resp);
  }
}

export default ListAllUsersController;
