import { NextFunction, Request, Response } from "express";

import { DeleteUserService } from "../../services/users";

class DeleteUserController {
  async handle(request: Request, response: Response, next: NextFunction) {
    try {
      const { uuid } = request.params;

      const deleteUserService = new DeleteUserService();

      const partner = await deleteUserService.execute(uuid);

      return response
        .status(200)
        .json({ message: "User removido com sucesso!" });
    } catch (err) {
      next(err);
    }
  }
}

export default DeleteUserController;
