import { Request, Response, NextFunction } from "express";
import { CreateUserService } from "../../services/users";

class CreateUserController {
  async handle(request: Request, response: Response, next: NextFunction) {
    try {
      const { name, email, birthday, password, gender, cell_number } =
        request.body;

      const createUserService = new CreateUserService();

      const user = await createUserService.execute({
        name,
        email,
        password,
        cell_number,
        birthday,
        gender,
      });
      const { password_hash, ...data } = user;

      return response.status(201).json(data);
    } catch (err) {
      next(err);
    }
  }
}

export default CreateUserController;
