import { NextFunction, Request, Response } from "express";

import { UpdateUserService } from "../../services/users";

class UpdateUserController {
  async handle(request: Request, response: Response, next: NextFunction) {
    try {
      const { uuid } = request.params;
      const data = request.body;

      const updateUserService = new UpdateUserService();

      const user = await updateUserService.execute({ uuid, data });

      const { password_hash, ...res }: any = user;

      return response.json(res);
    } catch (err) {
      next(err);
    }
  }
}

export default UpdateUserController;
