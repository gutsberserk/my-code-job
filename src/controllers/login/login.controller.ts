import { NextFunction, Request, Response } from "express";

import LoginService from "../../services/login/login.service";

class LoginController {
  async handle(request: Request, response: Response, next: NextFunction) {
    try {
      const { email, password } = request.body;

      const loginService = new LoginService();

      const token = await loginService.execute({ email, password });
      response.send({ token });
    } catch (err) {
      next(err);
    }
  }
}

export default LoginController;
