import request from "supertest";
import { createConnection, getConnection } from "typeorm";
import app from "../../app";
import config from "../../database";

describe("Testing the user CRUD", () => {
  beforeAll(async () => {
    await createConnection(config);
  });

  afterAll(async () => {
    const defaultConnection = getConnection("default");
    await defaultConnection.close();
  });

  let token = "";
  let userId = "";

  it("Should be able to create a new user", async () => {
    const response = await request(app).post("/user").send({
      name: "EXAMPLE USER",
      email: "example@mail.com.br",
      password: "123456",
      cell_number: "555555555",
      birthday: "2000-07-08",
      gender: "male",
    });

    userId = response.body.uuid;

    expect(response.status).toBe(201);
  });

  it("Should be able to login with the created user", async () => {
    const response = await request(app).post("/login").send({
      email: "example@mail.com",
      password: "123456",
    });

    token = response.body.token;

    expect(response.status).toBe(200);
  });

  it("Should be able to get the user profile informations", async () => {
    const response = await request(app)
      .get(`/user/${userId}`)
      .set({ Authorization: `Bearer ${token}` });

    expect(response.status).toBe(200);
  });

  it("Should be able to update the created user", async () => {
    const response = await request(app)
      .patch(`/user/${userId}`)
      .send({
        name: "Test user updated",
      })
      .set({ Authorization: `Bearer ${token}` });

    expect(response.status).toBe(200);
  });

  it("Should be able to delete the created user", async () => {
    const response = await request(app)
      .delete(`/user/${userId}`)
      .set({ Authorization: `Bearer ${token}` });

    expect(response.status).toBe(200);
  });
});
