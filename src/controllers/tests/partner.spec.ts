import request from "supertest";
import { createConnection, getConnection } from "typeorm";
import app from "../../app";
import config from "../../database";

describe("Testing the user CRUD partner", () => {
  beforeAll(async () => {
    await createConnection(config);
  });

  afterAll(async () => {
    const defaultConnection = getConnection("default");
    await defaultConnection.close();
  });

  let token = "";
  let partnerId = "";

  it("Should be able to create a new partner", async () => {
    const response = await request(app).post("/partner").send({
      name: "EXAMPLE COMPANY",
      email: "exampl@mail.com",
      password: "1234",
      location: "SP",
      website: "www.examplecompany.com",
    });

    partnerId = response.body.uuid;

    expect(response.status).toBe(201);
  });

  it("Should be able to login with the created partner", async () => {
    const response = await request(app).post("/login").send({
      email: "exampl@mail.com",
      password: "1234",
    });

    token = response.body.token;

    expect(response.status).toBe(200);
  });

  it("Should be able to get the partner profile informations", async () => {
    const response = await request(app)
      .get(`/partner/${partnerId}`)
      .set({ Authorization: `Bearer ${token}` });

    expect(response.status).toBe(200);
  });

  it("Should be able to update the created partner", async () => {
    const response = await request(app)
      .patch(`/partner/${partnerId}`)
      .send({
        name: "Test user updated",
      })
      .set({ Authorization: `Bearer ${token}` });

    expect(response.status).toBe(200);
  });

  it("Should be able to delete the created partner", async () => {
    const response = await request(app)
      .delete(`/partner/${partnerId}`)
      .set({ Authorization: `Bearer ${token}` });

    expect(response.status).toBe(200);
  });
});
