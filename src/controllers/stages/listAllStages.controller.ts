import { NextFunction, Request, Response } from "express";

import { ListAllStagesService } from "../../services/stages";

class ListAllStagesController {
  async handle(request: Request, response: Response, next: NextFunction) {
    try {
      const jobId = request.params.uuid;
      const userId = request.userId;

      const getAllStageService = new ListAllStagesService();

      const stages = await getAllStageService.execute(userId, jobId);

      response.send({ data: stages });
    } catch (err) {
      next(err);
    }
  }
}

export default ListAllStagesController;
