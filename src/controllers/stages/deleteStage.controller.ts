import { NextFunction, Request, Response } from "express";

import { DeleteStageService } from "../../services/stages";

class DeleteStageController {
  async handle(request: Request, response: Response, next: NextFunction) {
    try {
      const stageId = request.params.uuid;

      const deleteStageSerice = new DeleteStageService();

      await deleteStageSerice.execute(stageId);

      response.send({ message: "Stage foi removido." });
    } catch (err) {
      next(err);
    }
  }
}

export default DeleteStageController;
