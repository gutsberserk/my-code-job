import { NextFunction, Request, Response } from "express";

import { CreateStageService } from "../../services/stages";

class CreateStageController {
  async handle(request: Request, response: Response, next: NextFunction) {
    try {
      const data = request.body;
      const jobId = request.params.uuid;
      const userId = request.userId;

      const createStageService = new CreateStageService();

      const stage = await createStageService.execute(jobId, userId, data);
      response.status(200).send({ data: stage });
    } catch (err) {
      next(err);
    }
  }
}

export default CreateStageController;
