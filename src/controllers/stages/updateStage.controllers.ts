import { NextFunction, Request, Response } from "express";

import { UpdateStageService } from "../../services/stages";

class UpdateStageController {
  async handle(request: Request, response: Response, next: NextFunction) {
    try {
      const stageId = request.params.uuid;
      const data = request.body;

      const updateStageService = new UpdateStageService();

      const stage = await updateStageService.execute(stageId, data);
      response.send({ data: stage });
    } catch (err) {
      next(err);
    }
  }
}

export default UpdateStageController;
