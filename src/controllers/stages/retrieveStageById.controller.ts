import { NextFunction, Request, Response } from "express";

import { RetrieveStageByIdService } from "../../services/stages";

class RetrieveStageByIdController {
  async handle(request: Request, response: Response, next: NextFunction) {
    try {
      const stageId = request.params.uuid;

      const getOneStageService = new RetrieveStageByIdService();

      const stage = await getOneStageService.execute(stageId);

      response.send({ data: stage });
    } catch (err) {
      next(err);
    }
  }
}

export default RetrieveStageByIdController;
