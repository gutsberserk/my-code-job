export const config = {
  secret: "secret_key",
  expiresIn: "24h",
};
export const configRecover = {
  secret: "secret_key",
  expiresIn: "1h",
};
