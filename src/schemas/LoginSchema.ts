import * as yup from "yup";
import { SchemaOf } from "yup";
import { ILoginRequest } from "../types";

const LoginSchema: SchemaOf<ILoginRequest> = yup.object().shape({
  email: yup
    .string()
    .email("E-mail deve ser inserido no formato correto: nome + @ + domínio")
    .required("email é necessário para o login.")
    .transform((value, originalValue) => {
      return originalValue.toLowerCase();
    }),
  password: yup.string().required("password é necessário para o login."),
});

export default LoginSchema;
