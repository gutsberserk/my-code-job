import * as yup from "yup";
import { SchemaOf } from "yup";

interface IJobCreate {
  name: string;
  description: string;
  plataform?: string;
  location: string;
  salary?: string;
  benefits?: string;
  format: string;
  requiredTimeOfXP?: string;
  requiredXP?: string;
  differentialXP?: string;
  hireMode: string;
  link: string;
  hasTest?: boolean;
}

const jobCreateSchema: SchemaOf<IJobCreate> = yup.object().shape({
  name: yup.string().required(),
  description: yup
    .string()
    .required()
    .min(3, "Descrição precisa de ao menos: 3 caracteres")
    .max(255, "Limite máximo de caracteres: 255 caracteres."),
  plataform: yup.string(),
  location: yup.string().required(),
  salary: yup.string(),
  benefits: yup.string(),
  format: yup.string().required(),
  requiredTimeOfXP: yup.string(),
  requiredXP: yup.string(),
  differentialXP: yup.string(),
  hireMode: yup.string().required(),
  link: yup.string().required(),
  hasTest: yup.bool(),
});

export default jobCreateSchema;
