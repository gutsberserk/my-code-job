import * as yup from "yup";
import { SchemaOf } from "yup";
import { IPartnerCreate } from "../types";

const PartnerSchema: SchemaOf<IPartnerCreate> = yup.object().shape({
  name: yup.string().required(),
  email: yup
    .string()
    .email("E-mail deve ser inserido no formato correto: nome + @ + domínio")
    .required("email é necessário para o login.")
    .transform((value, originalValue) => {
      return originalValue.toLowerCase();
    }),
  password: yup.string().required("password é necessário para o login."),
  location: yup
    .string()
    .required()
    .min(2, "É necessário ter pelo menos 2 caracteres."),
  website: yup.string().required(),
});

export default PartnerSchema;
