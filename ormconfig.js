const developmentEnv = {
  type: "postgres",
  url: process.env.DATABASE_URL,
  cli: {
    entitiesDir: "src/entities",
    migrationsDir: "src/migrations",
  },
  ssl:
    process.env.NODE_ENV === "production"
      ? { rejectUnauthorized: false }
      : false,
  entities:
    process.env.NODE_ENV === "production"
      ? ["dist/entities/**/*.js"]
      : ["src/entities/**/*.ts"],
  migrations:
    process.env.NODE_ENV === "production"
      ? ["dist/migrations/**/*.js"]
      : ["src/migrations/**/*.ts"],
  synchronize: true,
  logging: false,
};

module.exports = developmentEnv;
